// Logger describes the log functionality.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logger

import (
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/stringsbuilder"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"
)

// Message describes a log message.
type Message struct {
	Identifier string      // The process name
	LogName    string      // The destination log
	Message    string      // The message
	Data       interface{} // Optional extra data
}

// Standard strings used to replace data that fails to convert to/from JSON.
const (
	UnableToMarshal   = "Unable to convert data to JSON"
	UnableToUnmarshal = "Unable to convert data from JSON"
)

// MessageFunc can be called to log a message. Note that no return error does not guarantee that the message was logged: a MessageFunc is free to silently discarded messages rather than block.
type MessageFunc func(ctx context.Context, m Message) error

// Logger wraps a MessageFunc to provide a logger satisfying log.Interface.
type Logger struct {
	identifier string      // The process name
	logName    string      // The destination log
	f          MessageFunc // The destination for log messages
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// writeJSON writes the JSON-encoded key: value pair to b. If first is false then this pair will be prefixed with a comma.
func writeJSON(b *bytes.Buffer, key string, value interface{}, first bool) error {
	if !first {
		b.WriteByte(',')
	}
	b.WriteByte('"')
	b.WriteString(key)
	b.WriteByte('"')
	b.WriteByte(':')
	d, err := json.Marshal(value)
	if err != nil {
		return err
	}
	b.Write(d)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Message functions
/////////////////////////////////////////////////////////////////////////

// MarshalJSON returns a JSON description of the Message.
func (m Message) MarshalJSON() ([]byte, error) {
	// Create the bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Write the opening brace
	b.WriteByte('{')
	first := true
	// Write the identifier
	if len(m.Identifier) != 0 {
		if err := writeJSON(b, "Identifier", m.Identifier, first); err != nil {
			return nil, err
		}
		first = false
	}
	// Write the log name
	if len(m.LogName) != 0 {
		if err := writeJSON(b, "LogName", m.LogName, first); err != nil {
			return nil, err
		}
		first = false
	}
	// Write the message
	if len(m.Message) != 0 {
		if err := writeJSON(b, "Message", m.Message, first); err != nil {
			return nil, err
		}
		first = false
	}
	// Write the data
	if m.Data != nil {
		if !first {
			b.WriteByte(',')
		}
		b.WriteString("\"Data\":")
		b.Write(MarshalJSONData(m.Data))
	}
	// Write the closing brace
	b.WriteByte('}')
	// Return a copy of the underlying bytes
	raw := b.Bytes()
	cpy := make([]byte, len(raw))
	copy(cpy, raw)
	return cpy, nil
}

// String returns a string description of the Message.
func (m Message) String() string {
	// Create the strings builder
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Write the identifier
	first := true
	if len(m.Identifier) != 0 {
		b.WriteString(m.Identifier)
		b.WriteByte(':')
		first = false
	}
	// Write the log name
	if len(m.LogName) != 0 {
		if !first {
			b.WriteByte(' ')
		}
		b.WriteString(m.LogName)
		b.WriteByte(':')
		first = false
	}
	// Write the message
	if len(m.Message) != 0 {
		if !first {
			b.WriteByte(' ')
		}
		b.WriteString(m.Message)
	}
	return b.String()
}

// Scan copies the contents of the Message src into dest. Any previously set data in dest will be deleted or overwritten. Note that the Data will be a shallow copy.
func Scan(dst *Message, src Message) {
	dst.Identifier = src.Identifier
	dst.LogName = src.LogName
	dst.Message = src.Message
	dst.Data = src.Data
}

/////////////////////////////////////////////////////////////////////////
// Logger functions
/////////////////////////////////////////////////////////////////////////

// New returns a new logger with given identifier and log name, where all generated log messages are passed to the given MessageFunc. A nil MessageFunc will discard all log messages.
func New(identifier string, logName string, f MessageFunc) *Logger {
	return &Logger{
		identifier: identifier,
		logName:    logName,
		f:          f,
	}
}

// send attempts to send the given message and any additional user-provided data. Note that no return error does not guarantee that the message was logged: a MessageFunc is free to silently discarded messages rather than block.
func (l *Logger) send(ctx context.Context, message string, data interface{}) (err error) {
	if l != nil && l.f != nil {
		// Recover from any panics
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("panic in MessageFunc: %s", e)
			}
		}()
		// Pass the message to the MessageFunc
		err = l.f(ctx, Message{
			Identifier: l.Identifier(),
			LogName:    l.LogName(),
			Message:    message,
			Data:       data,
		})
	}
	return
}

// Identifier returns the identifier that will be included with every log message.
func (l *Logger) Identifier() string {
	if l == nil {
		return ""
	}
	return l.identifier
}

// LogName returns the log name that will be included with every log message.
func (l *Logger) LogName() string {
	if l == nil {
		return ""
	}
	return l.logName
}

// Print logs the given message. Its arguments are handled in the manner of fmt.Print.
func (l *Logger) Print(v ...interface{}) {
	l.PrintContext(context.Background(), v...) // Ignore any error
}

// PrintContext is similar to Print, except the provided context will be passed to the MessageFunc, and any error is returned.
func (l *Logger) PrintContext(ctx context.Context, v ...interface{}) error {
	n := len(v)
	args := make([]string, 0, n)
	for i := 0; i < n; i++ {
		args = append(args, "%v")
	}
	return l.PrintfContext(ctx, strings.Join(args, " "), v...)
}

// Println logs the given message. Its arguments are handled in the manner of fmt.Println.
func (l *Logger) Println(v ...interface{}) {
	l.PrintlnContext(context.Background(), v...) // Ignore any error
}

// PrintlnContext is similar to Println, except the provided context will be passed to the MessageFunc, and any error is returned.
func (l *Logger) PrintlnContext(ctx context.Context, v ...interface{}) error {
	return l.PrintContext(ctx, v...)
}

// Printf logs the given message. Its arguments are handled in the manner of fmt.Printf.
func (l *Logger) Printf(format string, v ...interface{}) {
	l.PrintfContext(context.Background(), format, v...) // Ignore any error
}

// PrintfContext is similar to Printf, except the provided context will be passed to the MessageFunc, and any error is returned.
func (l *Logger) PrintfContext(ctx context.Context, format string, v ...interface{}) error {
	return l.send(ctx, fmt.Sprintf(format, v...), nil)
}

// JSON prints a log message to the logger, including JSON data specified by the first argument. The first argument should be capable of being marshalled into JSON via json.Marshal in package "encoding/json". The last two arguments define a message to be logged alongside data, and are handled in the manner of fmt.Printf. The values passed in data overwrite any values with the same key names set using SetOption.
func (l *Logger) JSON(data interface{}, format string, v ...interface{}) {
	l.JSONContext(context.Background(), data, format, v...) // Ignore any error
}

// JSONContext is similar to JSON, except the provided context will be passed to the MessageFunc, and any error is returned.
func (l *Logger) JSONContext(ctx context.Context, data interface{}, format string, v ...interface{}) error {
	return l.send(ctx, fmt.Sprintf(format, v...), data)
}

// String returns a string representation of the logger.
func (l *Logger) String() string {
	// Create a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Start building the string
	b.WriteString("Logger{Identifier: \"")
	b.WriteString(l.Identifier())
	b.WriteString("\", LogName: \"")
	b.WriteString(l.LogName())
	b.WriteByte('"')
	// Add the closing brace and return the string
	b.WriteByte('}')
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// MarshalJSONData marshals the given data to JSON. If the data will not marshal then a standard JSON-encoded error message is returned in its place.
func MarshalJSONData(data interface{}) []byte {
	b, err := json.Marshal(data)
	if err != nil {
		return []byte("{\"error\":\"" + UnableToMarshal + "\"}")
	}
	return b
}

// UnmarshalJSONData unmarshals the given data from JSON. If the data will not unmarshal then a standard error message is returned in its place.
func UnmarshalJSONData(data string) interface{} {
	var d interface{}
	if json.Unmarshal([]byte(data), &d) != nil {
		d = map[string]interface{}{
			"error": UnableToUnmarshal,
		}
	}
	return d
}
