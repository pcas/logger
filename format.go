// Format provides a common way of reading/writing log messages.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logger

import (
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/stringsbuilder"
	"encoding/json"
	"errors"
	"io"
	"strings"
)

// Formatter provides a way of converting log messages to strings, and strings back to log messages.
type Formatter interface {
	// WriteTo writes the given Message to w. The return value n is the
	// number of bytes written. Any error encountered during the write is also
	// returned.
	WriteTo(w io.Writer, msg Message) (n int64, err error)
	// Scan scans the given string s to the Message dest. Any previously set
	// data in dest will be deleted or overwritten.
	Scan(dest *Message, s string) error
}

// defaultFormat is the default formatter for log messages.
type defaultFormat int

// DefaultFormat is the default Formatter for log messages. Messages are formatted as:
//   identifier:message [DATA:JSON-encoded data]
// The identifier is the Identifier of the message (and may be an empty string), but with every occurrence of ":" escaped as "\:". The message is equal to the Message (and may be an empty string). Any occurrence of the substring "DATA:" in the message is escaped as "\DATA:". The finally DATA section will only be included if the message has non-empty Data, and is presented in compact JSON format.
const DefaultFormat = defaultFormat(0)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// writeByte writes the given byte to w.
func writeByte(w io.Writer, b byte) error {
	if bw, ok := w.(io.ByteWriter); ok {
		return bw.WriteByte(b)
	}
	_, err := w.Write([]byte{b})
	return err
}

// unescapedIndexOfByte return the index of the first unescaped instance of c in s, or -1 if an unescaped c is not present in s.
func unescapedIndexOfByte(s string, c byte) int {
	isEscaped := false
	return strings.IndexFunc(s, func(r rune) bool {
		if !isEscaped && r == rune(c) {
			return true
		}
		isEscaped = r == '\\'
		return false
	})
}

// unescapedIndex return the index of the first unescaped instance of substr in s, or -1 if an unescaped substr is not present in s.
func unescapedIndex(s string, substr string) int {
	offset := 0
	idx := strings.Index(s, substr)
	for idx != -1 {
		// Was this escaped?
		if idx == 0 || s[idx-1] != '\\' {
			return idx + offset
		}
		// Move on
		offset += idx + len(substr)
		s = s[idx+len(substr):]
		idx = strings.Index(s, substr)
	}
	return -1
}

// marshalCompactJSONData marshals the given data into compact JSON. If the data will not marshal then a standard JSON-encoded error message is returned in its place.
func marshalCompactJSONData(data interface{}) []byte {
	// Create a bytes buffer
	buf := bytesbuffer.New()
	defer bytesbuffer.Reuse(buf)
	// Compact the JSON
	if err := json.Compact(buf, MarshalJSONData(data)); err != nil {
		return []byte("{\"error\":\"" + UnableToMarshal + "\"}")
	}
	// Make a copy of the underlying bytes and return
	b := buf.Bytes()
	c := make([]byte, len(b))
	copy(c, b)
	return c
}

/////////////////////////////////////////////////////////////////////////
// defaultFormat functions
/////////////////////////////////////////////////////////////////////////

// WriteTo writes the given Message to w. The return value n is the number of bytes written. Any error encountered during the write is also returned.
func (defaultFormat) WriteTo(w io.Writer, msg Message) (n int64, err error) {
	// Escape the identifier and write it to w
	if len(msg.Identifier) != 0 {
		id := strings.Replace(msg.Identifier, ":", "\\:", -1)
		k, e := io.WriteString(w, id)
		n += int64(k)
		if e != nil {
			err = e
			return
		}
	}
	// Write the separator
	if err = writeByte(w, ':'); err != nil {
		return
	}
	n++
	// Escape the message and write it to w
	if len(msg.Message) != 0 {
		m := strings.Replace(msg.Message, "DATA:", "\\DATA:", -1)
		k, e := io.WriteString(w, m)
		n += int64(k)
		if e != nil {
			err = e
			return
		}
	}
	// Write the data
	if msg.Data != nil {
		// Write the separator
		k, e := io.WriteString(w, " DATA:")
		n += int64(k)
		if e != nil {
			err = e
			return
		}
		// Marshal the data and write it to w
		k, err = w.Write(marshalCompactJSONData(msg.Data))
		n += int64(k)
	}
	return
}

// Scan scans the given string s to the Message dest. Any previously set data in dest will be deleted or overwritten.
func (defaultFormat) Scan(dest *Message, s string) error {
	// Find the end of the identifier
	idx := unescapedIndexOfByte(s, ':')
	if idx == -1 {
		return errors.New("malformed string (expected ':')")
	}
	// Note the unescaped identifier and truncate s
	id := strings.Replace(s[:idx], "\\:", ":", -1)
	s = s[idx+1:]
	// Find the end of the message
	idx = unescapedIndex(s, "DATA:")
	// Note the unescaped message and truncate s
	var msg string
	if idx == -1 {
		msg = strings.Replace(s, "\\DATA:", "DATA:", -1)
		s = ""
	} else {
		msg = strings.Replace(s[:idx], "\\DATA:", "DATA:", -1)
		s = strings.TrimSpace(s[idx+5:])
	}
	// Attempt to JSON-decode the data section
	var data interface{}
	if len(s) != 0 {
		data = UnmarshalJSONData(s)
	}
	// Looks good
	dest.Identifier = strings.TrimSpace(id)
	dest.Message = strings.TrimSpace(msg)
	dest.Data = data
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToString converts the given Message to a string using the given formatter.
func ToString(msg Message, f Formatter) (s string, err error) {
	if f == nil {
		err = errors.New("illegal nil Formatter")
		return
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	if _, err = f.WriteTo(b, msg); err == nil {
		s = b.String()
	}
	return
}

// FromString converts the given string to a Message using the given formatter.
func FromString(s string, f Formatter) (Message, error) {
	if f == nil {
		return Message{}, errors.New("illegal nil Formatter")
	}
	dest := Message{}
	if err := f.Scan(&dest, s); err != nil {
		return Message{}, err
	}
	return dest, nil
}
