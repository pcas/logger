module bitbucket.org/pcas/logger

go 1.13

require (
	bitbucket.org/pcas/sslflag v0.0.16
	bitbucket.org/pcastools/address v0.1.4
	bitbucket.org/pcastools/bytesbuffer v1.0.3
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/contextutil v1.0.3
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/gobutil v1.0.4
	bitbucket.org/pcastools/grpcutil v1.0.14
	bitbucket.org/pcastools/hash v1.0.5
	bitbucket.org/pcastools/listenutil v0.0.10
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/pool v1.0.4
	bitbucket.org/pcastools/rand v1.0.4
	bitbucket.org/pcastools/stringsbuilder v1.0.3
	bitbucket.org/pcastools/ulid v0.1.6
	bitbucket.org/pcastools/version v0.0.5
	github.com/Shopify/sarama v1.33.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/stretchr/testify v1.8.1
	google.golang.org/grpc v1.54.0
	google.golang.org/protobuf v1.30.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
