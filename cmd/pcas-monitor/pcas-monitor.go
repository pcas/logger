// Pcas-monitor implements a monitor for pcas-monitord.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Create the monitor connection
	var c monitor.Connection
	if c, err = monitord.NewClient(ctx, opts.ClientConfig); err != nil {
		return
	}
	// Defer closing the connection
	defer func() {
		if cl, ok := c.(io.Closer); ok {
			if closeErr := cl.Close(); err == nil {
				err = closeErr
			}
		}
	}()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the command
	if opts.Run != nil {
		err = opts.Run(ctx, c)
	}
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
