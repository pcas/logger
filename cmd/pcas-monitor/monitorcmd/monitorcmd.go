// Monitorcmd implements the standard monitor commands.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitorcmd

import (
	"bitbucket.org/pcas/logger/monitor"
	"context"
	"fmt"
	"os"
	"sort"
	"strings"
)

// Cmd represents a command.
type Cmd struct {
	name  string
	desc  string
	parse parseFunc
}

// RunFunc is a function that can be run on the given connection to execute a command.
type RunFunc func(context.Context, monitor.Connection) error

// parseFunc is a parse function for a command.
type parseFunc func([]string) (RunFunc, error)

// registry contains the registered commands.
var registry map[string]*Cmd

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// register registers the a new command with given name, description, and parse function. This will panic if the name is already in use.
func register(name string, desc string, parse parseFunc) {
	if registry == nil {
		registry = make(map[string]*Cmd)
	}
	if _, ok := registry[name]; ok {
		panic("Command already registered with name " + name)
	}
	registry[name] = &Cmd{
		name:  name,
		desc:  desc,
		parse: parse,
	}
}

// assertNoUnusedArgs asserts that the given slice of arguments is empty. If the assertion fails then a help message will be printed to os.Stderr, the given usage function will be called (if non-nil), and the process will terminate via os.Exit(1).
func assertNoUnusedArgs(args []string, usage func()) {
	if n := len(args); n == 0 {
		return
	} else if n == 1 {
		fmt.Fprintf(os.Stderr, "1 unused argument provided: %s\n", args[0])
	} else {
		fmt.Fprintf(os.Stderr, "%d unused arguments provided: %s\n", n, strings.Join(args, ", "))
	}
	if usage != nil {
		usage()
	}
	os.Exit(1)
}

/////////////////////////////////////////////////////////////////////////
// Cmd functions
/////////////////////////////////////////////////////////////////////////

// String returns the command name.
func (c *Cmd) String() string {
	if c == nil {
		return ""
	}
	return c.name
}

// Description returns a brief one-line description of the command.
func (c *Cmd) Description() string {
	if c == nil {
		return ""
	}
	return c.desc
}

// Parse attempts to generate a run function from the given command-line arguments.
func (c *Cmd) Parse(args []string) (RunFunc, error) {
	if c == nil || c.parse == nil {
		return nil, nil
	}
	return c.parse(args)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Get returns the command with the given name, if any.
func Get(name string) (*Cmd, bool) {
	c, ok := registry[name]
	return c, ok
}

// Cmds returns a slice of all available commands, sorted in increasing order by command name.
func Cmds() []*Cmd {
	// Create the slice of available commands
	cmds := make([]*Cmd, 0, len(registry))
	for _, c := range registry {
		cmds = append(cmds, c)
	}
	// Sort the slice and return
	sort.Slice(cmds, func(i int, j int) bool {
		return cmds[i].String() < cmds[j].String()
	})
	return cmds
}
