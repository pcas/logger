// Entryprinter implements outputting of log entries.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitorcmd

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcastools/bytesbuffer"
	"encoding/csv"
	"encoding/json"
	"io"
	"time"
)

// DefaultTimestampFormat is the default format for outputting a timestamp.
const DefaultTimestampFormat = "2006-01-02 15:04:05.000"

// EntryPrinter is the interface satisfied by an object that can output log entries.
type EntryPrinter interface {
	WriteEntry(monitor.Entry) error // Write writes the given entry.
}

// tabprinter outputs log entries in tab-separated format.
type tabprinter struct {
	w      io.Writer // The underlying writer
	format string    // The timestamp format
}

// csvprinter outputs log entries in csv format.
type csvprinter struct {
	w      *csv.Writer // The underlying CSV writer
	buf    []string    // A buffer for writing record data
	format string      // The timestamp format
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// writeByte writes the given byte to w.
func writeByte(w io.Writer, b byte) error {
	if bw, ok := w.(io.ByteWriter); ok {
		return bw.WriteByte(b)
	}
	_, err := w.Write([]byte{b})
	return err
}

/////////////////////////////////////////////////////////////////////////
// tabprinter functions
/////////////////////////////////////////////////////////////////////////

// NewTabPrinter returns a new EntryPrinter that uses tabs to separate the information in a log entry. The timestamp will be formatted according to 'format'.
func NewTabPrinter(w io.Writer, format string) EntryPrinter {
	return &tabprinter{
		w:      w,
		format: format,
	}
}

// WriteEntry writes the given entry.
func (p *tabprinter) WriteEntry(e monitor.Entry) error {
	// Write the timestamp
	tstr := e.T.In(time.Local).Format(p.format)
	if _, err := io.WriteString(p.w, tstr); err != nil {
		return err
	} else if err = writeByte(p.w, '\t'); err != nil {
		return err
	}
	// Write the identifier
	if _, err := io.WriteString(p.w, e.Msg.Identifier); err != nil {
		return err
	} else if err = writeByte(p.w, '\t'); err != nil {
		return err
	}
	// Write the message
	if _, err := io.WriteString(p.w, e.Msg.Message); err != nil {
		return err
	}
	// If present, write the data
	if e.Msg.Data != nil {
		if err := writeByte(p.w, '\t'); err != nil {
			return err
		}
		// Marshal the data into compact JSON
		b := bytesbuffer.New()
		defer bytesbuffer.Reuse(b)
		err := json.Compact(b, logger.MarshalJSONData(e.Msg.Data))
		if err != nil {
			return err
		}
		// Write the bytes buffer to w
		if _, err = b.WriteTo(p.w); err != nil {
			return err
		}
	}
	// Write a new line
	return writeByte(p.w, '\n')
}

/////////////////////////////////////////////////////////////////////////
// csvprinter functions
/////////////////////////////////////////////////////////////////////////

// NewCSVPrinter returns a new EntryPrinter that outputs log entries in CSV format. The timestamp will be formatted according to 'format'.
func NewCSVPrinter(w io.Writer, format string) EntryPrinter {
	return &csvprinter{
		w:      csv.NewWriter(w),
		buf:    make([]string, 4),
		format: format,
	}
}

// WriteEntry writes the given entry.
func (p *csvprinter) WriteEntry(e monitor.Entry) error {
	// Populate the buffer for this entry
	p.buf[0] = e.T.In(time.Local).Format(p.format)
	p.buf[1] = e.Msg.Identifier
	p.buf[2] = e.Msg.Message
	p.buf[3] = ""
	// If present, add the data to the buffer
	if e.Msg.Data != nil {
		// Marshal the data into compact JSON
		b := bytesbuffer.New()
		defer bytesbuffer.Reuse(b)
		err := json.Compact(b, logger.MarshalJSONData(e.Msg.Data))
		if err != nil {
			return err
		}
		p.buf[3] = b.String()
	}
	// Write the buffer to the CSV writer
	if err := p.w.Write(p.buf); err != nil {
		return err
	}
	// Flush the CSV writer
	p.w.Flush()
	return p.w.Error()
}
