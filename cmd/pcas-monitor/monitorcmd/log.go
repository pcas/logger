// Log implements the "log" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitorcmd

import (
	"bitbucket.org/pcas/logger/monitor"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
	"time"
)

// bufferSize is the iterator buffer size
const bufferSize = 1024

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("log", "Fetch log messages from the specified log.", parseLog)
}

// parseLog parses the given arguments for the command, returning a run function on success.
func parseLog(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("log", flag.ExitOnError)
	f.Usage = usageLog
	// Set the flags
	var csv bool
	f.BoolVar(&csv, "csv", false, "")
	var after, before string
	f.StringVar(&after, "from", "", "")
	f.StringVar(&after, "f", "", "")
	f.StringVar(&before, "to", "", "")
	f.StringVar(&before, "t", "", "")
	var identifierSubstring, identifierRegexp string
	f.StringVar(&identifierSubstring, "si", "", "")
	f.StringVar(&identifierRegexp, "ei", "", "")
	var identifierNotSubstring, identifierNotRegexp string
	f.StringVar(&identifierNotSubstring, "Si", "", "")
	f.StringVar(&identifierNotRegexp, "Ei", "", "")
	var messageSubstring, messageRegexp string
	f.StringVar(&messageSubstring, "sm", "", "")
	f.StringVar(&messageRegexp, "em", "", "")
	var messageNotSubstring, messageNotRegexp string
	f.StringVar(&messageNotSubstring, "Sm", "", "")
	f.StringVar(&messageNotRegexp, "Em", "", "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Extract the name
	args = f.Args()
	if len(args) == 0 {
		return nil, errors.New("the log name must be specified")
	}
	name, args := args[0], args[1:]
	// Sanity check
	assertNoUnusedArgs(args, f.Usage)
	// Parse the start time
	ok, start := monitor.ParseStartTime(after, time.Local)
	if !ok && len(after) != 0 {
		return nil, errors.New("unable to parse -from (" + after + ")")
	}
	// Parse the finish time
	ok, finish := monitor.ParseEndTime(before, time.Local)
	if !ok && len(before) != 0 {
		return nil, errors.New("unable to parse -to (" + before + ")")
	}
	// Create the conditions to match
	cond := monitor.Matches{
		MessageSubstring:       messageSubstring,
		MessageNotSubstring:    messageNotSubstring,
		MessageRegexp:          messageRegexp,
		MessageNotRegexp:       messageNotRegexp,
		IdentifierSubstring:    identifierSubstring,
		IdentifierNotSubstring: identifierNotSubstring,
		IdentifierRegexp:       identifierRegexp,
		IdentifierNotRegexp:    identifierNotRegexp,
	}
	// Create the entry printer
	var p EntryPrinter
	if csv {
		p = NewCSVPrinter(os.Stderr, DefaultTimestampFormat)
	} else {
		p = NewTabPrinter(os.Stderr, DefaultTimestampFormat)
	}
	// Return the run function
	return createLogFunc(name, start, finish, cond, p), nil
}

// usageLog writes a usage message describing the arguments for the command.
func usageLog() {
	fmt.Fprint(os.Stderr, `Command "log" outputs messages from the named log.

Command usage: log [flags] name

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -csv\tOutput the entries in CSV format.\n")
	fmt.Fprint(w, "  -ei\tFilter for identifiers that match this regular expression.\n")
	fmt.Fprint(w, "  -Ei\tFilter for identifiers that do not match this regular expression.\n")
	fmt.Fprint(w, "  -em\tFilter for messages that match this regular expression.\n")
	fmt.Fprint(w, "  -Em\tFilter for messages that do not match this regular expression.\n")
	fmt.Fprint(w, "  -from, -f\tFetch messages from this point (inclusive).\n")
	fmt.Fprint(w, "  -help, -h\tPrint this message and exit.\n")
	fmt.Fprint(w, "  -si\tFilter for identifiers that contain this string.\n")
	fmt.Fprint(w, "  -Si\tFilter for identifiers that do not contain this string.\n")
	fmt.Fprint(w, "  -sm\tFilter for messages that contain this string.\n")
	fmt.Fprint(w, "  -Sm\tFilter for messages that do not contain this string.\n")
	fmt.Fprint(w, "  -to, -t\tFetch messages up to this point (inclusive).\n")
	w.Flush()
	fmt.Fprint(os.Stderr, `
Notes
-----
The filters -ei, -Ei, -si, and -Si filter against the identifier, whilst the
filters -em, -Em, -sm, and -Sm filter against the message. All of these filters
may be applied simultaneously.

If -from is specified then only messages logged at or after this point will be
fetched; the default is to fetch all messages in the log. If -to is specified
then only messages logged at or before this point will be fetched; the default
is to continue fetching messages from the log as they come in.

The values of -from and -to are very flexible. Valid formats include:
`)
	fmt.Fprint(w, "  \"start\"/\"end\"\tThe first/most recent message in the log.\n")
	fmt.Fprint(w, "  \"now\"\tThe current time.\n")
	fmt.Fprint(w, "  \"today\"\tThe start/end of today.\n")
	fmt.Fprint(w, "  \"tomorrow\"\tThe start/end of tomorrow.\n")
	fmt.Fprint(w, "  \"yesterday\"\tThe start/end of yesterday.\n")
	fmt.Fprint(w, "  day of the week\tThe start/end of the most recent occurrence of that day.\n")
	fmt.Fprint(w, "  month\tThe start/end of the most recent occurrence of that month.\n")
	fmt.Fprint(w, "  timestamp\tThat time (example: \"Jun 22 15:25:17\").\n")
	fmt.Fprint(w, "  duration\tThat duration before the current time (example: \"30m\").\n")
	fmt.Fprint(w, "  integer N\tN number of messages before the most recent message.\n")
	w.Flush()
	fmt.Fprint(os.Stderr, `
A negative duration gives a time in the future. For example, "-1h" corresponds
to the time 1 hour from now. Similarly, if the integer N is negative then it is
interpreted to mean |N| messages after the most recent message.

Examples
--------
log -from 0 -to 0 name
log -from 0 -to now name
These examples are equivalent. Output the most recent message from "name".

log -from 9 -to 0 name
Output the 10 most recent messages from "name".

log -from 1h -to now name
Output all messages produced within the last hour from "name".

log -from today -to now name
Output all of today's messages so far from "name".

log -from now -to -1m name
Output all messages from "name" received in the next minute.

log -from -1 -to -2 name
Output the next two messages to arrive in "name".
`)
}

// isContextError returns true iff err is a context error.
func isContextError(err error) bool {
	return err == context.Canceled || err == context.DeadlineExceeded
}

// createLogFunc returns a run function to output the matching log entries to the EntryPrinter p.
func createLogFunc(name string, start *monitor.Time, finish *monitor.Time, cond monitor.Matches, p EntryPrinter) RunFunc {
	return func(ctx context.Context, c monitor.Connection) (err error) {
		// Open the stream
		var itr monitor.Iterator
		itr, err = monitor.StreamWithMatches(ctx, c, name, start, finish, cond)
		if err != nil {
			return
		}
		// Wrap the iterator in a buffer
		itr = monitor.NewBufferedIterator(itr, bufferSize)
		// Defer closing the iterator and checking for iteration errors
		defer func() {
			if closeErr := itr.Close(); closeErr != nil {
				if err == nil || isContextError(err) {
					err = closeErr
				}
			}
			if itrErr := itr.Err(); itrErr != nil {
				if err == nil || isContextError(err) {
					err = itrErr
				}
			}
		}()
		// Output the log entries
		err = outputIterator(ctx, p, itr)
		return
	}
}

// outputIterator outputs the entries from the given iterator to the EntryPrinter p. It is the caller's responsibility to close the iterator when finished with it.
func outputIterator(ctx context.Context, p EntryPrinter, itr monitor.Iterator) error {
	e := monitor.Entry{}
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		if err = itr.Scan(&e); err != nil {
			return err
		} else if err = p.WriteEntry(e); err != nil {
			return err
		}
		ok, err = itr.NextContext(ctx)
	}
	return nil
}
