// List implements the "list" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitorcmd

import (
	"bitbucket.org/pcas/logger/monitor"
	"context"
	"flag"
	"fmt"
	"os"
	"sort"
	"text/tabwriter"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("list", "List the available logs.", parseList)
}

// parseList parses the given arguments for the command, returning a run function on success.
func parseList(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("list", flag.ExitOnError)
	f.Usage = usageList
	// Set the flags
	var substr, reg string
	f.StringVar(&substr, "s", "", "")
	f.StringVar(&reg, "e", "", "")
	var notSubstr, notReg string
	f.StringVar(&notSubstr, "S", "", "")
	f.StringVar(&notReg, "E", "", "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Sanity check
	assertNoUnusedArgs(f.Args(), f.Usage)
	// Create the conditions to match
	cond := monitor.NameMatches{
		Substring:    substr,
		NotSubstring: notSubstr,
		Regexp:       reg,
		NotRegexp:    notReg,
	}
	// Return the run function
	return createListFunc(cond), nil
}

// usageList writes a usage message describing the arguments for the command.
func usageList() {
	fmt.Fprint(os.Stderr, `Command "list" outputs the available log names.

Command usage: list [flags]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -e\tFilter for log names that match this regular expression.\n")
	fmt.Fprint(w, "  -E\tFilter for log names that do not match this regular expression.\n")
	fmt.Fprint(w, "  -help, -h\tPrint this message and exit.\n")
	fmt.Fprint(w, "  -s\tFilter for log names that contain this string.\n")
	fmt.Fprint(w, "  -S\tFilter for log names that do not contain this string.\n")
	w.Flush()
}

// createListFunc returns a run function to output the list of available logs.
func createListFunc(cond monitor.NameMatches) RunFunc {
	return func(ctx context.Context, c monitor.Connection) error {
		// Fetch the log names
		names, err := monitor.LogNamesWithMatches(ctx, c, cond)
		if err != nil {
			return err
		}
		// Sort the log names in increasing lex order
		sort.StringSlice(names).Sort()
		// Output the log names
		for _, name := range names {
			fmt.Printf("%s\n", name)
		}
		return nil
	}
}
