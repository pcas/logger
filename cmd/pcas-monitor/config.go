// Config.go handles configuration and logging for pcas-monitor

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/cmd/pcas-monitor/monitorcmd"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
)

// Options describes the options.
type Options struct {
	*monitord.ClientConfig
	Run monitorcmd.RunFunc // The run function for the command
}

// Name is the name of the executable.
const Name = "pcas-monitor"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		ClientConfig: monitord.DefaultConfig(),
	}
}

// validate validates the options.
func validate(opts *Options) error {
	return opts.Validate()
}

// commandFlagSet returns a dummy flag set, with the empty set of flags and a usage headers and footer that describe the commands
func commandFlagSet() flag.Set {
	cmds := flag.NewBasicSet("Commands")
	var usage strings.Builder
	w := tabwriter.NewWriter(&usage, 12, 1, 2, ' ', 0)
	for i, c := range monitorcmd.Cmds() {
		if i != 0 {
			fmt.Fprintf(w, "\n")
		}
		fmt.Fprintf(w, "  %s\t%s", c.String(), c.Description())
	}
	w.Flush()
	cmds.SetUsageHeader(usage.String())
	cmds.SetUsageFooter(fmt.Sprintf("Help for a specified command can be obtained via:\n  %s cmd -h", Name))
	return cmds
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s monitors Kafka logs.\n\nUsage: %s [options] [cmd [cmd flags] [cmd args]]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address,
			"The address to bind to",
			address.EnvUsage("address", "PCAS_MONITOR_ADDRESS")),
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Add a dummy flag set that provides a description of the commands
	flag.AddSet(commandFlagSet())
	// Parse the flags
	flag.Parse()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Check that a command is specified
	args := flag.Args()
	if len(args) == 0 {
		return errors.New("no command specified")
	}
	cmd, ok := monitorcmd.Get(args[0])
	if !ok {
		return errors.New("unknown command: " + args[0])
	}
	// Parse the command
	var err error
	if opts.Run, err = cmd.Parse(args[1:]); err != nil {
		return err
	}
	// Set the logger
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
