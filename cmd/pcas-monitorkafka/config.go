// Config.go handles configuration and logging for pcas-monitorkafka

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/cmd/pcas-monitor/monitorcmd"
	"bitbucket.org/pcas/logger/monitor/kafka"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
)

// Options describes the options.
type Options struct {
	Run         monitorcmd.RunFunc // The run function for the command
	BrokerAddrs string             // Comma-separated list of broker addresses for the Kafka consumers
}

// Name is the name of the executable.
const Name = "pcas-monitorkafka"

// The default values for the arguments.
const (
	DefaultBrokerAddrs = kafka.DefaultBrokerAddr
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default brokers
	brokers, ok := os.LookupEnv("PCAS_KAFKA_BROKERS")
	if !ok {
		brokers = DefaultBrokerAddrs
	}
	// Return the default options
	return &Options{
		BrokerAddrs: brokers,
	}
}

// commandFlagSet returns a dummy flag set, with the empty set of flags and a usage headers and footer that describe the commands
func commandFlagSet() flag.Set {
	cmds := flag.NewBasicSet("Commands")
	var usage strings.Builder
	w := tabwriter.NewWriter(&usage, 12, 1, 2, ' ', 0)
	for i, c := range monitorcmd.Cmds() {
		if i != 0 {
			fmt.Fprintf(w, "\n")
		}
		fmt.Fprintf(w, "  %s\t%s", c.String(), c.Description())
	}
	w.Flush()
	cmds.SetUsageHeader(usage.String())
	cmds.SetUsageFooter(fmt.Sprintf("Help for a specified command can be obtained via:\n  %s cmd -h", Name))
	return cmds
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf(`%s monitors Kafka logs.

Usage: %s [options] [cmd [cmd flags] [cmd args]]`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("broker", &opts.BrokerAddrs, opts.BrokerAddrs, "List of Kafka brokers", "The flag -broker can be set to a comma-separated list of Kafka brokers.  Its default value can be specified using the environment variable PCAS_KAFKA_BROKERS."),
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		&version.Flag{AppName: Name},
	)
	// Add a dummy flag set that provides a description of the commands
	flag.AddSet(commandFlagSet())
	// Parse the flags
	flag.Parse()
	// Check that a command is specified
	args := flag.Args()
	if len(args) == 0 {
		return errors.New("no command specified")
	}
	cmd, ok := monitorcmd.Get(args[0])
	if !ok {
		return errors.New("unknown command: " + args[0])
	}
	// Parse the command
	var err error
	if opts.Run, err = cmd.Parse(args[1:]); err != nil {
		return err
	}
	// Set the logger and return success
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
