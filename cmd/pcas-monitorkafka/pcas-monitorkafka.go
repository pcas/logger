// Monitorkafka implements a monitor for Kafka.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/logger/monitor/kafka"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
	"os"
	"strings"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// connectToMonitor returns the monitor connection.
func connectToMonitor(opts *Options, lg log.Interface) (monitor.Connection, error) {
	// Create the slice of broker addresses
	addrs := strings.Split(opts.BrokerAddrs, ",")
	// Provide some logging feedback
	lg.Printf(`Communicating with Kafka using settings:
  broker=%s`,
		strings.Join(addrs, ","))
	// Establish a connection to Kafka
	c, err := kafka.New(kafka.AddBrokerAddr(addrs...))
	if err != nil {
		return nil, err
	}
	c.SetLogger(lg)
	// Close the connection on cleanup and return
	cleanup.Add(c.Close)
	return c, nil
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Create the monitor connection
	var c monitor.Connection
	if c, err = connectToMonitor(opts, lg); err != nil {
		return err
	}
	// Defer closing the connection
	defer func() {
		if cl, ok := c.(io.Closer); ok {
			if closeErr := cl.Close(); err == nil {
				err = closeErr
			}
		}
	}()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the command
	if opts.Run != nil {
		err = opts.Run(ctx, c)
	}
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
