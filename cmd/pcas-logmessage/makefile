BINARY = pcas-logmessage
PLATFORMS = linux-amd64 darwin-arm64 darwin-amd64

DATE ?= $(shell date -u '+%Y-%m-%dT%TZ')
VERSION ?= $(shell git describe --tags --abbrev=0 --match="v*" --dirty 2> /dev/null || echo v0.0.0-dirty)
COMMIT ?= $(shell git rev-parse --short HEAD)

MODULE = $(shell $(GO) list -m)
PKGS = $(shell $(GO) list ./...)
TESTPKGS = $(shell $(GO) list -f \
			'{{ if or .TestGoFiles .XTestGoFiles }}{{ .ImportPath }}{{ end }}' \
			$(PKGS))

define newline


endef

null :=
space := ${null} ${null}
${space} := ${space}

VMODULE = bitbucket.org/pcastools/version
SSLMODULE = bitbucket.org/pcas/sslflag
LDFLAGS = -X $(VMODULE).gitDescribe=$(VERSION) -X $(VMODULE).gitCommit=$(COMMIT) -X $(VMODULE).buildDate=$(DATE)
ifdef PCAS_DEVEL_SSL_CERT
	PCAS_DEVEL_SSL_CERT := $(subst $(newline),\n,$(PCAS_DEVEL_SSL_CERT))
	PCAS_DEVEL_SSL_CERT := $(subst ${ },_,$(PCAS_DEVEL_SSL_CERT))
	LDFLAGS := $(LDFLAGS) -X $(SSLMODULE).certificates=$(PCAS_DEVEL_SSL_CERT)
endif

target = $(word 1, $@)
os_arch_pair = $(subst -, ,$(target))
os = $(word 1, $(os_arch_pair))
arch = $(word 2, $(os_arch_pair))

RELEASEDIR = release
DSTDIR = $(RELEASEDIR)/$(target)

GO = go

V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell printf "\033[34;1m▶\033[0m")

.PHONEY: all
all: clean test release

.PHONEY: test
test:
	$(info $(M) testing $(BINARY)…)
	$(Q) $(GO) test $(TESTPKGS)

.PHONEY: build
build:
	$(info $(M) building $(BINARY)…)
	$(Q) $(GO) build -ldflags '$(LDFLAGS)' -o $(BINARY)

.PHONEY: $(PLATFORMS)
$(PLATFORMS):
	$(info $(M) building $(DSTDIR)/$(BINARY)…)
	$(Q) mkdir -p $(DSTDIR)
	$(Q) GOOS=$(os) GOARCH=$(arch) go build -ldflags '$(LDFLAGS)' -o $(DSTDIR)/$(BINARY)
	$(Q) shasum -b -a 256 $(DSTDIR)/$(BINARY) | cut -d" " -f1 > $(DSTDIR)/sha256
	$(Q) gzip $(DSTDIR)/$(BINARY)
	$(Q) mv $(DSTDIR)/$(BINARY).gz $(DSTDIR)/$(BINARY)-$(VERSION).gz

.PHONY: clean
clean:
	@rm -f $(BINARY)
	@rm -rf $(RELEASEDIR)

.PHONY: release
release: $(PLATFORMS)

.PHONEY: version
version:
	@echo $(VERSION)
