// Config.go handles configuration and logging.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
	"strings"
)

// Options describes the options.
type Options struct {
	*logd.ClientConfig
	Message    string // The message
	LogName    string // The log name
	Identifier string // The identifier
}

// Name is the name of the executable.
const Name = "pcas-logmessage"

// The default values for the arguments.
const (
	DefaultLogName = Name
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default identifier
	identifier, err := os.Hostname()
	if err != nil {
		identifier = Name
	}
	// Return the default options structure
	return &Options{
		ClientConfig: logd.DefaultConfig(),
		LogName:      DefaultLogName,
		Identifier:   identifier,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if err := opts.Validate(); err != nil {
		return err
	} else if len(strings.TrimSpace(opts.LogName)) == 0 {
		return errors.New("the log name must not be empty")
	} else if len(strings.TrimSpace(opts.Identifier)) == 0 {
		return errors.New("the identifier must not be empty")
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s sends a message to the pcas log server.\n\nUsage: %s [options] message", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address,
			"The address of the pcas log server",
			address.EnvUsage("address", "PCAS_LOG_ADDRESS")),
		flag.String("id", &opts.Identifier, opts.Identifier, "The identifier to use", ""),
		flag.Bool("log-to-stderr", &logToStderr, logToStderr, "Log to stderr", ""),
		flag.String("name", &opts.LogName, opts.LogName, "The name of the log to use", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Check that there is a message
	if flag.NArg() == 0 {
		return errors.New("you must provide a log message")
	}
	opts.Message = strings.Join(flag.Args(), " ")
	// Set the loggers
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
