// Config.go handles configuration and logging for the pcas-logd server

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/kafka"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"fmt"
	"math"
	"os"
	"runtime"
)

// Options describes the options.
type Options struct {
	// Logd server options
	Address           *address.Address // The address to bind to
	SSLKey            []byte           // The SSL private key
	SSLKeyCert        []byte           // The SSL private key certificate
	MaxNumConnections int              // The maximum number of connections
	// Kafka options
	BrokerAddrs  string // Comma-separated list of broker addresses for the Kafka producers
	BufferSize   int    // The size of the message buffer
	NumProducers int    // The number of Kafka producers
	NumWorkers   int    // The number of workers feeding log messages to the producers
	// General options
	Debug bool // Enable more verbose log messages
}

// Name is the name of the executable.
const Name = "pcas-logd"

// The default values for the arguments.
const (
	DefaultHostname          = "localhost"
	DefaultPort              = logd.DefaultTCPPort
	DefaultMaxNumConnections = 1024
	DefaultBufferSize        = 65536
	DefaultBrokerAddrs       = kafka.DefaultBrokerAddr
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Create the default brokers
	brokers, ok := os.LookupEnv("PCAS_KAFKA_BROKERS")
	if !ok {
		brokers = DefaultBrokerAddrs
	}
	// Create the default address
	addr, err := address.NewTCP(DefaultHostname, DefaultPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Return the default options
	return &Options{
		Address:           addr,
		MaxNumConnections: DefaultMaxNumConnections,
		BrokerAddrs:       brokers,
		BufferSize:        DefaultBufferSize,
		NumProducers:      runtime.NumCPU(),
		NumWorkers:        2 * runtime.NumCPU(),
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (%d)", opts.MaxNumConnections)
	} else if opts.BufferSize <= 0 || opts.BufferSize > math.MaxInt32 {
		return fmt.Errorf("invalid buffer size (%d)", opts.BufferSize)
	} else if opts.NumProducers <= 0 || opts.NumProducers > 4096 {
		return fmt.Errorf("the number of producers (%d) must be an integer between 1 and 4096", opts.NumProducers)
	} else if opts.NumWorkers <= 0 || opts.NumWorkers > 4096 {
		return fmt.Errorf("the number of workers (%d) must be an integer between 1 and 4096", opts.NumWorkers)
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is the pcas log server.\n\nUsage: %s [flags]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		address.NewFlag("address", &opts.Address, opts.Address, "The address to bind to", "The value of the flag -address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\"."),
		flag.String("broker", &opts.BrokerAddrs, opts.BrokerAddrs, "List of Kafka brokers", "The flag -broker can be set to a comma-separated list of Kafka brokers.  Its default value can be specified using the environment variable PCAS_KAFKA_BROKERS."),
		flag.Int("buffer-size", &opts.BufferSize, opts.BufferSize, "The log message buffer size", ""),
		flag.Bool("debug", &opts.Debug, false, "Increase the amount of log output", ""),
		flag.Bool("log-to-stderr", &logToStderr, false, "Log to stderr", ""),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		flag.Int("num-producers", &opts.NumProducers, opts.NumProducers, "The number of Kafka producers", ""),
		flag.Int("num-workers", &opts.NumWorkers, opts.NumWorkers, "The number of workers", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the loggers
	if opts.Debug && !logToStderr {
		fmt.Fprintf(os.Stderr, "%s: Logging not enabled, so -debug will have no effect\n", Name)
	}
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
