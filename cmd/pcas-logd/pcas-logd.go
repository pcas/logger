// Logd implements a logging server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/kafka"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
)

// logMessager is the interface satisfied by the LogMessage method.
type logMessager interface {
	LogMessage(ctx context.Context, m logger.Message) error
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// connectToLogger returns the destination logger.
func connectToLogger(opts *Options, lg log.Interface) (logMessager, error) {
	// Create the slice of broker addresses
	addrs := strings.Split(opts.BrokerAddrs, ",")
	// Provide some logging feedback
	lg.Printf(`Communicating with Kafka using settings:
  broker=%s
  buffer-size=%d
  num-producers=%d
  num-workers=%d`,
		strings.Join(addrs, ","),
		opts.BufferSize,
		opts.NumProducers,
		opts.NumWorkers,
	)
	// Create the log
	if opts.Debug {
		lg = log.PrefixWith(lg, "[kafka]")
	} else {
		lg = log.Discard
	}
	// Establish a connection to Kafka
	return kafka.New(
		kafka.AddBrokerAddr(addrs...),
		kafka.BufferSize(opts.BufferSize),
		kafka.NumProducers(opts.NumProducers),
		kafka.NumWorkers(opts.NumWorkers),
		kafka.DebugLog(lg),
	)
}

// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := logd.DefaultTCPPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, opts *Options, lg log.Interface) (net.Listener, error) {
	port := logd.DefaultWSPort
	if a.HasPort() {
		port = a.Port()
	}
	uri := "ws://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	l, shutdown, err := listenutil.WebsocketListenAndServe(uri,
		listenutil.MaxNumConnections(opts.MaxNumConnections),
		listenutil.Log(lg),
	)
	if err != nil {
		return nil, err
	}
	cleanup.Add(shutdown)
	return l, nil
}

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (l net.Listener, err error) {
	switch opts.Address.Scheme() {
	case "tcp":
		l, err = createTCPListener(opts.Address, opts, lg)
	case "ws":
		l, err = createWebsocketListener(opts.Address, opts, lg)
	default:
		err = errors.New("unsupported URI scheme: " + opts.Address.Scheme())
	}
	if err != nil {
		lg.Printf("Error starting listener: %v", err)
	}
	return
}

// run starts up and runs the server. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, k logMessager, lg log.Interface) error {
	// Build the options
	serverOpts := []logd.Option{
		logd.MessageFunc(k.LogMessage),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, logd.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := logd.NewServer(serverOpts...)
	if err != nil {
		return err
	}
	s.SetLogger(log.PrefixWith(lg, "[logdserver]"))
	// If the context fires, stop the server
	go func() {
		<-ctx.Done()
		s.GracefulStop()
	}()
	// Create the listener and serve any incoming connections
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	return s.Serve(l)
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Create the connection to the logger
	var k logMessager
	if k, err = connectToLogger(opts, lg); err != nil {
		return
	}
	// Defer closing the logger
	defer func() {
		if cl, ok := k.(io.Closer); ok {
			if closeErr := cl.Close(); err == nil {
				err = closeErr
			}
		}
	}()
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, k, lg)
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
