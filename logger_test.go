// Tests of logger.go.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logger

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestString tests the String method.
func TestString(t *testing.T) {
	assert := assert.New(t)
	// Check we get the strings we expect
	assert.Equal(
		`FooBar: TheLog: This is a log message`,
		(&Message{
			Identifier: "FooBar",
			LogName:    "TheLog",
			Message:    "This is a log message",
			Data: map[string]interface{}{
				"a": 1,
			},
		}).String())
	assert.Equal(
		`FooBar: This is a log message`,
		(&Message{
			Identifier: "FooBar",
			Message:    "This is a log message",
		}).String())
	assert.Equal(
		`TheLog: This is a log message`,
		(&Message{
			LogName: "TheLog",
			Message: "This is a log message",
		}).String())
	assert.Equal(
		`This is a log message`,
		(&Message{
			Message: "This is a log message",
		}).String())
	assert.Equal(
		`FooBar: TheLog:`,
		(&Message{
			Identifier: "FooBar",
			LogName:    "TheLog",
		}).String())
	// Test the empty message
	var m Message
	assert.Equal("", m.String())
}

// TestJsonEncoding tests the JSON encoding.
func TestJsonEncoding(t *testing.T) {
	assert := assert.New(t)
	// Create a new message
	m := &Message{
		Identifier: "FooBar",
		LogName:    "TheLog",
		Message:    "This is a log message",
		Data: map[string]interface{}{
			"a": 1,
			"b": []string{"c", "d"},
			"e": true,
			"f": nil,
		},
	}
	// Encode a message as JSON
	b, err := json.Marshal(m)
	assert.NoError(err)
	// Now let's check we can decode the JSON
	m2 := &Message{}
	assert.NoError(json.Unmarshal(b, &m2))
	// Check that the easy stuff decoded correctly
	assert.Equal(m.Identifier, m2.Identifier)
	assert.Equal(m.LogName, m2.LogName)
	assert.Equal(m.Message, m2.Message)
	// Checking the contents of Data is a pain
	data1, ok := m.Data.(map[string]interface{})
	assert.True(ok)
	data2, ok := m2.Data.(map[string]interface{})
	assert.True(ok)
	assert.Equal(len(data1), len(data2))
	for k, v := range data1 {
		v2, ok := data2[k]
		assert.True(ok)
		switch k {
		case "a":
			assert.Equal(v, int(v2.(float64)))
		case "b":
			S1 := v.([]string)
			S2 := v2.([]interface{})
			assert.Equal(len(S1), len(S2))
			for i, s := range S1 {
				assert.Equal(s, S2[i].(string))
			}
		case "e":
			assert.Equal(v, v2)
		case "f":
			assert.Equal(v, v2)
		}
	}
	// Now we check that the JSON is encoding as expected
	b, err = json.Marshal(&Message{})
	assert.NoError(err)
	assert.Equal(`{}`, string(b))
	b, err = json.Marshal(&Message{Identifier: "Foo"})
	assert.NoError(err)
	assert.Equal(`{"Identifier":"Foo"}`, string(b))
	b, err = json.Marshal(&Message{LogName: "Foo"})
	assert.NoError(err)
	assert.Equal(`{"LogName":"Foo"}`, string(b))
}
