// Buffer provides a buffer for submitting log messages.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logger

import (
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"
)

// LogMessager is the interface satisfied by a LogMessage method.
type LogMessager interface {
	LogMessage(context.Context, Message) error
}

// Buffer provides a buffer around a LogMessager.
type Buffer struct {
	bufferTimeout time.Duration   // Timeout when buffer is full
	flushTimeout  time.Duration   // Timeout flushing the buffer on close
	doneC         <-chan struct{} // Closed when the worker exits
	errC          <-chan error    // Worker reports any error on exit
	m             sync.RWMutex    // Controls access to the following
	isClosed      bool            // Have we closed
	closeErr      error           // The error on close (if any)
	msgC          chan<- Message  // The message channel
}

// BufferOption sets options on a Buffer.
type BufferOption interface {
	apply(*bufferOptions)
}

// funcBufferOption wraps a function that modifies bufferOptions into an implementation of the BufferOption interface.
type funcBufferOption struct {
	f func(*bufferOptions)
}

// apply calls the wrapped function f on the given bufferOptions.
func (h *funcBufferOption) apply(do *bufferOptions) {
	h.f(do)
}

// newFuncBufferOption returns a funcBufferOption wrapping f.
func newFuncBufferOption(f func(*bufferOptions)) *funcBufferOption {
	return &funcBufferOption{
		f: f,
	}
}

// bufferOptions are the options on a Buffer.
type bufferOptions struct {
	BufferSize    int           // The size of the message buffer
	BufferTimeout time.Duration // Timeout when attempting to add a message to a full buffer
	SendTimeout   time.Duration // Timeout on attempting to send a message to the server
	FlushTimeout  time.Duration // Timeout on flushing the buffer on Close
}

// The default option values.
const (
	DefaultBufferSize    = 1024
	DefaultBufferTimeout = 3 * time.Millisecond
	DefaultSendTimeout   = 5 * time.Second
	DefaultFlushTimeout  = 10 * time.Second
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// execMessageFunc forwards the given message to the given message function, recovering from any panics.
func execMessageFunc(ctx context.Context, f MessageFunc, m Message) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in MessageFunc: %s", e)
		}
	}()
	err = f(ctx, m)
	return
}

// worker is a background worker that reads messages from msgC and forwards them to the LogMessager l. Any errors returned by f will be ignored. The worker will exit when the msgC channel is closed. On exit: first doneC will be closed; then if the wrapped LogMessager satisfies the io.Closer interface then its Close method will be called, with any error passed down the errC channel; finally the errC channel will be closed.
func worker(l LogMessager, timeout time.Duration, msgC <-chan Message, errC chan<- error, doneC chan<- struct{}) {
	// Defer shutdown
	defer func() {
		close(doneC)
		if cl, ok := l.(io.Closer); ok {
			if err := cl.Close(); err != nil {
				errC <- err
			}
		}
		close(errC)
	}()
	// Iterator over the message channel
	for m := range msgC {
		// Attempt to forward the message
		sendCtx, cancel := context.WithTimeout(context.Background(), timeout)
		execMessageFunc(sendCtx, l.LogMessage, m) // Ignore any error
		cancel()
	}
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseBufferOptions parses the given optional functions.
func parseBufferOptions(option ...BufferOption) *bufferOptions {
	// Create the default options
	opts := &bufferOptions{
		BufferSize:    DefaultBufferSize,
		BufferTimeout: DefaultBufferTimeout,
		SendTimeout:   DefaultSendTimeout,
		FlushTimeout:  DefaultFlushTimeout,
	}
	// Set the options
	for _, h := range option {
		h.apply(opts)
	}
	return opts
}

// BufferSize sets the size of the message buffer.
func BufferSize(size int) BufferOption {
	return newFuncBufferOption(func(opts *bufferOptions) {
		if size < 0 {
			size = 0
		}
		opts.BufferSize = size
	})
}

// BufferTimeout sets the timeout when attempting to add a message to a full buffer.
func BufferTimeout(d time.Duration) BufferOption {
	return newFuncBufferOption(func(opts *bufferOptions) {
		opts.BufferTimeout = d
	})
}

// BufferSendTimeout sets the timeout on attempting to send a message to the wrapped LogMessager.
func BufferSendTimeout(d time.Duration) BufferOption {
	return newFuncBufferOption(func(opts *bufferOptions) {
		opts.SendTimeout = d
	})
}

// BufferFlushTimeout sets the timeout on flushing the buffer on Close.
func BufferFlushTimeout(d time.Duration) BufferOption {
	return newFuncBufferOption(func(opts *bufferOptions) {
		opts.FlushTimeout = d
	})
}

//////////////////////////////////////////////////////////////////////
// Buffer functions
//////////////////////////////////////////////////////////////////////

// NewBuffer returns a buffered logger to l.
func NewBuffer(l LogMessager, options ...BufferOption) *Buffer {
	// Parse the options
	opts := parseBufferOptions(options...)
	// Create the communication channels
	doneC, errC := make(chan struct{}), make(chan error, 1)
	msgC := make(chan Message, opts.BufferSize)
	// Start the background worker
	go worker(l, opts.SendTimeout, msgC, errC, doneC)
	// Return the buffer
	return &Buffer{
		bufferTimeout: opts.BufferTimeout,
		flushTimeout:  opts.FlushTimeout,
		doneC:         doneC,
		errC:          errC,
		msgC:          msgC,
	}
}

// Close flushes and closes the buffer, preventing any further messages from being submitted. If the wrapped LogMessager satisfies the io.Closer interface then its Close method will also be called.
func (b *Buffer) Close() error {
	// Sanity check
	if b == nil {
		return nil
	}
	// Acquire a write lock
	b.m.Lock()
	defer b.m.Unlock()
	// Is there anything to do?
	if !b.isClosed {
		// Mark us as closed and close the message channel
		b.isClosed = true
		close(b.msgC)
		// Wait for the background worker to exit
		ctx, cancel := context.WithTimeout(context.Background(), b.flushTimeout)
		defer cancel()
		select {
		case <-b.doneC:
			b.closeErr = <-b.errC
		case <-ctx.Done():
			b.closeErr = errors.New("close timed out")
		}
	}
	return b.closeErr
}

// LogMessage attempts to submit the given message. Note that no return error does not guarantee that the message was sent: messages may be silently discarded to avoid blocking.
func (b *Buffer) LogMessage(ctx context.Context, m Message) error {
	// Sanity check
	if b == nil {
		return errors.New("uninitialised buffer")
	}
	// Acquire a read lock
	b.m.RLock()
	defer b.m.RUnlock()
	// Have we closed?
	if b.isClosed {
		return errors.New("connection closed")
	}
	// Pass the message down the message channel
	select {
	case b.msgC <- m:
	default:
		// The channel blocked -- we have another go with a time-out
		if b.bufferTimeout != 0 {
			ctx, cancel := context.WithTimeout(ctx, b.bufferTimeout)
			defer cancel()
			select {
			case b.msgC <- m:
			case <-ctx.Done():
				return errors.New("dropping message")
			}
		}
	}
	// The message made it onto the channel
	return nil
}

// NewLogger returns a new logger with given identifier and log name.
func (b *Buffer) NewLogger(identifier string, logName string) *Logger {
	return New(identifier, logName, b.LogMessage)
}
