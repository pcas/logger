// Server handles connections from a logd client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logd

import (
	"context"
	"fmt"

	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/logd/internal/logdrpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/emptypb"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that logd listens on.
const (
	DefaultTCPPort = 12354
	DefaultWSPort  = 80
)

// loggerServer implements the tasks on the gRPC server.
type loggerServer struct {
	logdrpc.UnimplementedLoggerServer
	log.BasicLogable
	f logger.MessageFunc // The destination for the log messages
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// loggerServer functions
/////////////////////////////////////////////////////////////////////////

// LogMessage performs the LogMessage task.
func (s *loggerServer) LogMessage(ctx context.Context, m *logdrpc.Message) (res *emptypb.Empty, err error) {
	// Recover from any panics
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in MessageFunc: %s", e)
		}
	}()
	// Convert the message and forwarded it to the message function
	var mm logger.Message
	if mm, err = logdrpc.ToMessage(m); err != nil {
		return
	} else if err = s.f(ctx, mm); err != nil {
		return
	}
	res = &emptypb.Empty{}
	return
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new log server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &loggerServer{f: opts.MessageFunc}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpclog.UnaryServerInterceptor(m.Log()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpclog.StreamServerInterceptor(m.Log()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the underlying gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	logdrpc.RegisterLoggerServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer: m,
		Server:      s,
	}, nil
}
