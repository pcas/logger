//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. logd.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logdrpc

import (
	"bitbucket.org/pcas/logger"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromMessage converts the given logger.Message to a *Message.
func FromMessage(m logger.Message) *Message {
	// Convert the data to JSON
	var src []byte
	if m.Data != nil {
		src = logger.MarshalJSONData(m.Data)
	}
	// Return the message object
	return &Message{
		Identifier: m.Identifier,
		LogName:    m.LogName,
		LogMessage: m.Message,
		Data:       string(src),
	}
}

// ToMessage converts the given *Message to a logger.Message.
func ToMessage(m *Message) (res logger.Message, err error) {
	// Attempt to convert the data (if any) from JSON
	var data interface{}
	if s := m.GetData(); len(s) != 0 {
		data = logger.UnmarshalJSONData(s)
	}
	// Create the message
	res = logger.Message{
		Identifier: m.GetIdentifier(),
		LogName:    m.GetLogName(),
		Message:    m.GetLogMessage(),
		Data:       data,
	}
	return
}
