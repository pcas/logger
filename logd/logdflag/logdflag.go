// Package logdflag provides a standard flag set for configuring the logger package and starting logging. Typical usage would be something like:
//
//	import(
//		"bitbucket.org/pcas/logger/logd/logdflag"
//      "bitbucket.org/pcastools/flag"
//		...
//	)
//
// // parseArgs parses the command-line flags and environment variables.
// func parseArgs() error {
// 	// Define the usage message and command-line flags
// 	flag.SetGlobalHeader(fmt.Sprintf("...header for the usage message...")
// 	flag.SetName("Options")
// 	flag.Add(
// 		flag.String("foo", ...),
//      ...
// 	)
//  // Add the flagset that controls the logger
//	logSet := logdflag.NewLogSet(nil)
// 	flag.AddSet(logSet)
// 	// Parse the flags
// 	flag.Parse()
//  // Do any additional sanity checks
//  ...
//	// Set the global logger, using the application name as the log name
//	if err := logdflag.SetLogger(
//		logSet.ToStderr(),
//		logSet.ToServer(),
//		logSet.ClientConfig(),
//		Name,
//	); err != nil {
//		return err
//	}
//  return nil
//  }
package logdflag

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"os"
	"strconv"
	"sync"
)

// The default identifier and controlling mutex.
var (
	defaultIDm sync.Mutex
	defaultID  string
)

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c    *logd.ClientConfig // The client config
	addr flag.Flag          // The address flag
}

// LogSet represents a set of command-line flags defined by a client config, and provides destination logging information.
type LogSet struct {
	*Set
	toServerFlag flag.Flag // The flag for logging to the pcas server
	toStderrFlag flag.Flag // The flag for logging to os.Stderr
	toServer     bool      // Whether to log to the pcas log server
	toStderr     bool      // Whether to log to os.Stderr
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createLogdLogger returns a buffered logger that logs to the pcas log server specified by c and given log name.
func createLogdLogger(ctx context.Context, c *logd.ClientConfig, name string) (log.Interface, error) {
	// Sanity check
	if c == nil {
		c = logd.DefaultConfig()
	}
	// Create the new logd client
	client, err := logd.NewClient(ctx, c)
	if err != nil {
		return nil, err
	}
	// Wrap the client in a buffer and close it at cleanup
	b := logger.NewBuffer(client)
	cleanup.Add(b.Close)
	// Return the log
	return b.NewLogger(DefaultIdentifier(), name), nil
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the logd.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *logd.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = logd.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the address flags
	addr := address.NewFlag(
		"log-address",
		&c.Address, c.Address,
		"The address of the pcas log server",
		address.EnvUsage("log-address", "PCAS_LOG_ADDRESS"),
	)
	// Return the set
	return &Set{
		c:    c,
		addr: addr,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{s.addr}
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "Logging options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *logd.ClientConfig {
	if s == nil {
		return logd.DefaultConfig()
	}
	return s.c.Copy()
}

/////////////////////////////////////////////////////////////////////////
// LogSet functions
/////////////////////////////////////////////////////////////////////////

// NewLogSet returns a set of command-line flags as described by NewSet. In addition, this set provides destination logging information.
func NewLogSet(c *logd.ClientConfig) *LogSet {
	// Create the set
	s := &LogSet{
		Set: NewSet(c),
	}
	// Create the flags
	s.toServerFlag = flag.Bool(
		"log-to-server",
		&s.toServer, s.toServer,
		"Log to the pcas log server",
		"",
	)
	s.toStderrFlag = flag.Bool(
		"log-to-stderr",
		&s.toStderr, s.toStderr,
		"Log to stderr", "",
	)
	// Return the set
	return s
}

// Flags returns the members of the set.
func (s *LogSet) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return append(
		s.Set.Flags(),
		[]flag.Flag{s.toServerFlag, s.toStderrFlag}...,
	)
}

// ToStderr returns true iff the user selected to log to os.Stderr. This should only be called after the set has been successfully validated.
func (s *LogSet) ToStderr() bool {
	return s != nil && s.toStderr
}

// ToServer returns true iff the user selected to log to the logd server. This should only be called after the set has been successfully validated.
func (s *LogSet) ToServer() bool {
	return s != nil && s.toServer
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultIdentifier returns the identifier set on the logd client. This is typically of the form "<hostname>-<PID>", however if the hostname cannot be determined then a ULID is used.
func DefaultIdentifier() string {
	// Acquire a lock on the default identifier
	defaultIDm.Lock()
	defer defaultIDm.Unlock()
	// If necessary, set the default identifier
	if len(defaultID) == 0 {
		if s, err := os.Hostname(); err == nil {
			defaultID = s + "-" + strconv.Itoa(os.Getpid())
		} else {
			id, _ := ulid.New()
			defaultID = id.String()
		}
	}
	// Return the default identifier
	return defaultID
}

// SetLogger sets the global logger log.Log() as specified by toStderr, toServer, the client config c, and the destination log name. This will also update the default client config to c. If necessary, a shutdown function will be registered with package "bitbucket.org/pcastools/cleanup".
func SetLogger(ctx context.Context, toStderr bool, toServer bool, c *logd.ClientConfig, name string) error {
	// Update the default client config
	if c != nil {
		logd.SetDefaultConfig(c)
	}
	// Create the loggers
	lgs := make([]log.Interface, 0, 2)
	// Are we logging to stderr?
	if toStderr {
		lgs = append(lgs, log.Stderr)
	}
	// Are we logging to the pcas log server?
	if toServer {
		lg, err := createLogdLogger(ctx, c, name)
		if err != nil {
			return err
		}
		lgs = append(lgs, lg)
	}
	// Set the loggers
	if n := len(lgs); n == 1 {
		log.SetLogger(lgs[0])
	} else if n > 1 {
		lg := &log.ToMany{}
		lg.Append(lgs...)
		log.SetLogger(lg)
	}
	return nil
}
