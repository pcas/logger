/*
Package monitor simplifies retrieval of log messages.

The minimum interface is Connection below. Some implementations of Connection might be able to offload the task of filtering results to the underlying storage engine, which has the potential to be much more efficient. A number of optional interfaces -- listed below -- are available and will be used by if appropriate.

The optional interfaces that can be exploited for filtering are:

	type IsLogNamer interface {
	    IsLogName(name string) (bool, error)
	}

	type LogNamesWithSubstringer interface {
	    LogNamesWithSubstring(match string) ([]string, bool)
	}

	type LogNamesWithRegexper interface {
	    LogNamesWithRegexp(expr string) ([]string, bool)
	}

	type LogNamesWithMatcheser interface {
		LogNamesWithMatches(NameMatches) ([]string, error)
	}

	type StreamWithFilterer interface {
	    StreamWithFilter(ctx context.Context, name string,
	      start *Time, finish *Time, filter FilterFunc) (Iterator, error)
	}

	type StreamWithMessageSubstringer interface {
	    StreamWithMessageSubstring(ctx context.Context, name string,
	      start *Time, finish *Time, match string) (Iterator, error)
	}

	type StreamWithMessageRegexper interface {
	    StreamWithMessageRegexp(ctx context.Context, name string,
	      start *Time, finish *Time, expr string) (Iterator, error)
	}

	type StreamWithIdentifierSubstringer interface {
	    StreamWithIdentifierSubstring(ctx context.Context, name string,
	      start *Time, finish *Time, match string) (Iterator, error)
	}

	type StreamWithIdentifierRegexper interface {
	    StreamWithIdentifierRegexp(ctx context.Context, name string,
	      start *Time, finish *Time, expr string) (Iterator, error)
	}

	type StreamWithMatcheser interface {
	    StreamWithMatches(ctx context.Context, name string,
	      start *Time, finish *Time, cond Matches) (Iterator, error)
	}

*/
package monitor

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcastools/stringsbuilder"
	"context"
	"regexp"
	"strings"
	"time"
)

// Entry represents an entry in a log.
type Entry struct {
	T   time.Time      `json:"timestamp,omitempty"` // The timestamp
	Msg logger.Message `json:"message,omitempty"`   // The log message
}

// Connection is the interface satisfied by a connection to a log.
type Connection interface {
	// LogNames returns the log names.
	LogNames(ctx context.Context) ([]string, error)
	// Stream returns an iterator down which entries from the log 'name' are
	// passed. Only entries falling into the range determined by start and
	// finish will be returned. If start is unassigned then the start of the
	// log will be used; if end is unassigned then no end is used. The
	// caller should ensure that the returned Iterator is closed, otherwise
	// a resource leak will result.
	Stream(ctx context.Context, name string, start *Time, finish *Time) (Iterator, error)
}

// isLogNamer is an optional interface.
type isLogNamer interface {
	IsLogName(context.Context, string) (bool, error)
}

// logNamesWithSubstringer is an optional interface.
type logNamesWithSubstringer interface {
	LogNamesWithSubstring(context.Context, string) ([]string, error)
}

// logNamesWithRegexper is an optional interface.
type logNamesWithRegexper interface {
	LogNamesWithRegexp(context.Context, string) ([]string, error)
}

// logNamesWithMatcheser is an optional interface.
type logNamesWithMatcheser interface {
	LogNamesWithMatches(context.Context, NameMatches) ([]string, error)
}

// streamWithFilterer is an optional interface.
type streamWithFilterer interface {
	StreamWithFilter(context.Context, string, *Time, *Time, FilterFunc) (Iterator, error)
}

// streamWithMessageSubstringer is an optional interface.
type streamWithMessageSubstringer interface {
	StreamWithMessageSubstring(context.Context, string, *Time, *Time, string) (Iterator, error)
}

// streamWithMessageRegexper is an optional interface.
type streamWithMessageRegexper interface {
	StreamWithMessageRegexp(context.Context, string, *Time, *Time, string) (Iterator, error)
}

// streamWithIdentifierSubstringer is an optional interface.
type streamWithIdentifierSubstringer interface {
	StreamWithIdentifierSubstring(context.Context, string, *Time, *Time, string) (Iterator, error)
}

// streamWithIdentifierRegexper is an optional interface.
type streamWithIdentifierRegexper interface {
	StreamWithIdentifierRegexp(context.Context, string, *Time, *Time, string) (Iterator, error)
}

// streamWithMatcheser is an optional interface.
type streamWithMatcheser interface {
	StreamWithMatches(context.Context, string, *Time, *Time, Matches) (Iterator, error)
}

// nameFilterFunc is a function used to filter log names.
type nameFilterFunc func(string) bool

// FilterFunc can be used to filter an entry. The filter function should return true iff the entry is to be kept.
//
// A nil-valued filter function should be interpreted as the absence of a filter, and hence is equivalent to a filter function that always returns true.
type FilterFunc func(Entry) bool

// NameMatches provides a way to filter for specific log names. Empty values will be interpreted as meaning no condition, so will be ignored.
type NameMatches struct {
	Substring    string `json:",omitempty"` // A substring all log names must contain
	NotSubstring string `json:",omitempty"` // A substring all log names must not contain
	Regexp       string `json:",omitempty"` // A regular expression all log names must match
	NotRegexp    string `json:",omitempty"` // A regular expression all log names must not match
}

// Matches provides a way to filter for specific entries. Empty values will be interpreted as meaning no condition, so will be ignored.
type Matches struct {
	MessageSubstring       string `json:",omitempty"` // A substring all messages must contain
	MessageNotSubstring    string `json:",omitempty"` // A substring all messages must not contain
	MessageRegexp          string `json:",omitempty"` // A regular expression all messages must match
	MessageNotRegexp       string `json:",omitempty"` // A regular expression all messages must not match
	IdentifierSubstring    string `json:",omitempty"` // A substring all identifiers must contain
	IdentifierNotSubstring string `json:",omitempty"` // A substring all identifiers must not contain
	IdentifierRegexp       string `json:",omitempty"` // A regular expression all identifiers must match
	IdentifierNotRegexp    string `json:",omitempty"` // A regular expression all identifiers must not match
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// buildFilterForNameMatches returns a function that matches against 'cond'.
func buildFilterForNameMatches(cond NameMatches) (nameFilterFunc, error) {
	// Collect together the filter functions describing the matches
	fs := make([]nameFilterFunc, 0, 4)
	if len(cond.Substring) != 0 {
		fs = append(fs, func(s string) bool {
			return strings.Contains(s, cond.Substring)
		})
	}
	if len(cond.NotSubstring) != 0 {
		fs = append(fs, func(s string) bool {
			return !strings.Contains(s, cond.NotSubstring)
		})
	}
	if len(cond.Regexp) != 0 {
		re, err := regexp.Compile(cond.Regexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, re.MatchString)
	}
	if len(cond.NotRegexp) != 0 {
		re, err := regexp.Compile(cond.NotRegexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, func(s string) bool {
			return !re.MatchString(s)
		})
	}
	// "And" the filter functions together
	return andNameFilterFuncs(fs...), nil
}

// andNameFilterFuncs returns the filter function corresponding to f_1 && f_2 && ... && f_n, where the f_i are the given arguments. As with &&, this will short-circuit evaluation if one of the f_i returns false.
func andNameFilterFuncs(f ...nameFilterFunc) nameFilterFunc {
	if n := len(f); n == 0 {
		return nil
	} else if n == 1 {
		return f[0]
	}
	return func(s string) bool {
		for _, filter := range f {
			if filter != nil && !filter(s) {
				return false
			}
		}
		return true
	}
}

// applyNamesFilter applies the given filter function to the given list of log names. Returns the filtered list.
func applyNamesFilter(names []string, filter nameFilterFunc) []string {
	if filter == nil || len(names) == 0 {
		return names
	}
	subnames := make([]string, 0, len(names))
	for _, s := range names {
		if filter(s) {
			subnames = append(subnames, s)
		}
	}
	return subnames
}

// execFilterFunc returns the result of applying the filter to the given entry, recovering from any panics. A panic in filter will result in false being returned.
func execFilterFunc(filter FilterFunc, entry Entry) (ok bool) {
	if filter == nil {
		ok = true
	} else {
		defer func() {
			if e := recover(); e != nil {
				ok = false
			}
		}()
		ok = filter(entry)
	}
	return
}

// buildFilterForMatches returns a filter function that matches against 'cond'.
func buildFilterForMatches(cond Matches) (FilterFunc, error) {
	// Collect together the filter functions describing the matches
	fs := make([]FilterFunc, 0, 8)
	if len(cond.MessageSubstring) != 0 {
		fs = append(fs, func(e Entry) bool {
			return strings.Contains(e.Msg.Message, cond.MessageSubstring)
		})
	}
	if len(cond.MessageNotSubstring) != 0 {
		fs = append(fs, func(e Entry) bool {
			return !strings.Contains(e.Msg.Message, cond.MessageNotSubstring)
		})
	}
	if len(cond.MessageRegexp) != 0 {
		re, err := regexp.Compile(cond.MessageRegexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, func(e Entry) bool {
			return re.MatchString(e.Msg.Message)
		})
	}
	if len(cond.MessageNotRegexp) != 0 {
		re, err := regexp.Compile(cond.MessageNotRegexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, func(e Entry) bool {
			return !re.MatchString(e.Msg.Message)
		})
	}
	if len(cond.IdentifierSubstring) != 0 {
		fs = append(fs, func(e Entry) bool {
			return strings.Contains(e.Msg.Identifier, cond.IdentifierSubstring)
		})
	}
	if len(cond.IdentifierNotSubstring) != 0 {
		fs = append(fs, func(e Entry) bool {
			return !strings.Contains(e.Msg.Identifier, cond.IdentifierNotSubstring)
		})
	}
	if len(cond.IdentifierRegexp) != 0 {
		re, err := regexp.Compile(cond.IdentifierRegexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, func(e Entry) bool {
			return re.MatchString(e.Msg.Identifier)
		})
	}
	if len(cond.IdentifierNotRegexp) != 0 {
		re, err := regexp.Compile(cond.IdentifierNotRegexp)
		if err != nil {
			return nil, err
		}
		fs = append(fs, func(e Entry) bool {
			return !re.MatchString(e.Msg.Identifier)
		})
	}
	// "And" the filter functions together
	return And(fs...), nil
}

/////////////////////////////////////////////////////////////////////////
// Entry functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the Entry.
func (e Entry) String() string {
	s := e.T.In(time.Local).Format("2006-01-02 15:04:05.000")
	if msg := e.Msg.String(); len(msg) != 0 {
		s += " " + msg
	}
	return s
}

// Scan copies the contents of the Entry src into dest. Any previously set data in dest will be deleted or overwritten.
func Scan(dst *Entry, src Entry) {
	dst.T = src.T
	logger.Scan(&dst.Msg, src.Msg)
}

/////////////////////////////////////////////////////////////////////////
// NameMatches functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the matches.
func (c NameMatches) String() string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	isFirst := true
	if len(c.Substring) != 0 {
		b.WriteString("name CONTAINS(")
		b.WriteString(c.Substring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.NotSubstring) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("name NOT CONTAINS(")
		b.WriteString(c.NotSubstring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.Regexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("name MATCHES(")
		b.WriteString(c.Regexp)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.NotRegexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("name NOT MATCHES(")
		b.WriteString(c.NotRegexp)
		b.WriteByte(')')
		isFirst = false
	}
	if isFirst {
		b.WriteString("name IS ANY")
	}
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Matches functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the matches.
func (c Matches) String() string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	isFirst := true
	if len(c.MessageSubstring) != 0 {
		b.WriteString("message CONTAINS(")
		b.WriteString(c.MessageSubstring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.MessageNotSubstring) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("message NOT CONTAINS(")
		b.WriteString(c.MessageNotSubstring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.MessageRegexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("message MATCHES(")
		b.WriteString(c.MessageRegexp)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.MessageNotRegexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("message NOT MATCHES(")
		b.WriteString(c.MessageNotRegexp)
		b.WriteByte(')')
		isFirst = false
	}
	if isFirst {
		b.WriteString("message IS ANY AND ")
	}
	isFirst = true
	if len(c.IdentifierSubstring) != 0 {
		b.WriteString("identifier CONTAINS(")
		b.WriteString(c.IdentifierSubstring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.IdentifierNotSubstring) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("identifier NOT CONTAINS(")
		b.WriteString(c.IdentifierNotSubstring)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.IdentifierRegexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("identifier MATCHES(")
		b.WriteString(c.IdentifierRegexp)
		b.WriteByte(')')
		isFirst = false
	}
	if len(c.IdentifierNotRegexp) != 0 {
		if !isFirst {
			b.WriteString(" AND ")
		}
		b.WriteString("identifier NOT MATCHES(")
		b.WriteString(c.IdentifierNotRegexp)
		b.WriteByte(')')
		isFirst = false
	}
	if isFirst {
		b.WriteString("identifier IS ANY")
	}
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// And returns the filter function corresponding to f_1 && f_2 && ... && f_n, where the f_i are the given arguments. As with &&, And will short-circuit evaluation if one of the f_i returns false.
func And(f ...FilterFunc) FilterFunc {
	if n := len(f); n == 0 {
		return nil
	} else if n == 1 {
		return f[0]
	}
	return func(entry Entry) bool {
		for _, filter := range f {
			if filter != nil && !filter(entry) {
				return false
			}
		}
		return true
	}
}

// Or returns the filter function corresponding to f_1 || f_2 || ... || f_n, where the f_i are the given arguments. As with ||, Or will short-circuit evaluation if one of the f_i returns true.
func Or(f ...FilterFunc) FilterFunc {
	if n := len(f); n == 0 {
		return nil
	} else if n == 1 {
		return f[0]
	}
	return func(entry Entry) bool {
		for _, filter := range f {
			if filter == nil || filter(entry) {
				return true
			}
		}
		return false
	}
}

// IsLogName returns true iff the given string is a log name of the given Connection c.
func IsLogName(ctx context.Context, c Connection, name string) (bool, error) {
	// Does the connection support this?
	if d, ok := c.(isLogNamer); ok {
		return d.IsLogName(ctx, name)
	}
	// We try to reduce the number of names we need to check
	var names []string
	var err error
	if d, ok := c.(logNamesWithSubstringer); ok {
		names, err = d.LogNamesWithSubstring(ctx, name)
	} else if d, ok := c.(logNamesWithMatcheser); ok {
		names, err = d.LogNamesWithMatches(ctx, NameMatches{
			Substring: name,
		})
	} else {
		names, err = c.LogNames(ctx)
	}
	if err != nil {
		return false, err
	}
	// Is the given log name in the slice of log names?
	for _, s := range names {
		if s == name {
			return true, nil
		}
	}
	return false, nil
}

// LogNamesWithSubstring is the same as LogNames, with the exception that log names are filtered for those containing the substring 'match'.
func LogNamesWithSubstring(ctx context.Context, c Connection, match string) ([]string, error) {
	// Is there anything to do?
	if len(match) == 0 {
		return c.LogNames(ctx)
	}
	// Does the connection support this?
	if d, ok := c.(logNamesWithSubstringer); ok {
		return d.LogNamesWithSubstring(ctx, match)
	} else if d, ok := c.(logNamesWithMatcheser); ok {
		return d.LogNamesWithMatches(ctx, NameMatches{
			Substring: match,
		})
	}
	// No luck -- fetch the slice of available log names and filter then
	names, err := c.LogNames(ctx)
	if err != nil {
		return nil, err
	}
	return applyNamesFilter(names, func(s string) bool {
		return strings.Contains(s, match)
	}), nil
}

// LogNamesWithRegexp is the same as LogNames, with the exception that log names are filtered for those matching the given regular expression.
func LogNamesWithRegexp(ctx context.Context, c Connection, expr string) ([]string, error) {
	// Does the connection support this?
	if d, ok := c.(logNamesWithRegexper); ok {
		return d.LogNamesWithRegexp(ctx, expr)
	} else if d, ok := c.(logNamesWithMatcheser); ok {
		return d.LogNamesWithMatches(ctx, NameMatches{
			Regexp: expr,
		})
	}
	// No luck -- compile the regular expression
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}
	// Fetch the slice of available log names and filter then
	names, err := c.LogNames(ctx)
	if err != nil {
		return nil, err
	}
	return applyNamesFilter(names, re.MatchString), nil
}

// LogNamesWithMatches is the same as LogNames, with the exception that log names are filtered against the conditions 'cond'.
func LogNamesWithMatches(ctx context.Context, c Connection, cond NameMatches) ([]string, error) {
	// Does the connection support this?
	if d, ok := c.(logNamesWithMatcheser); ok {
		return d.LogNamesWithMatches(ctx, cond)
	}
	// If we're matching against a substring, does c support this?
	if s := cond.Substring; len(s) != 0 {
		if d, ok := c.(logNamesWithSubstringer); ok {
			cond.Substring = ""
			filter, err := buildFilterForNameMatches(cond)
			if err != nil {
				return nil, err
			}
			names, err := d.LogNamesWithSubstring(ctx, s)
			if err != nil {
				return nil, err
			}
			return applyNamesFilter(names, filter), nil
		}
	}
	// If we're matching against a regular expression, does c support this?
	if s := cond.Regexp; len(s) != 0 {
		if d, ok := c.(logNamesWithRegexper); ok {
			cond.Regexp = ""
			filter, err := buildFilterForNameMatches(cond)
			if err != nil {
				return nil, err
			}
			names, err := d.LogNamesWithRegexp(ctx, s)
			if err != nil {
				return nil, err
			}
			return applyNamesFilter(names, filter), nil
		}
	}
	// No luck -- we'll have to do all the filtering ourselves
	filter, err := buildFilterForNameMatches(cond)
	if err != nil {
		return nil, err
	}
	names, err := c.LogNames(ctx)
	if err != nil {
		return nil, err
	}
	return applyNamesFilter(names, filter), nil
}

// StreamWithFilter is the same as Stream, with the exception that entries are filtered via a user-provided filter function. The caller should ensure that the returned Iterator is closed, otherwise a resource leak will result.
func StreamWithFilter(ctx context.Context, c Connection, name string, start *Time, finish *Time, filter FilterFunc) (Iterator, error) {
	// Is there anything to do?
	if filter == nil {
		return c.Stream(ctx, name, start, finish)
	}
	// Does the connection support this method?
	if d, ok := c.(streamWithFilterer); ok {
		return d.StreamWithFilter(ctx, name, start, finish, filter)
	}
	// No luck -- we need to do the filtering outselves
	itr, err := c.Stream(ctx, name, start, finish)
	if err != nil {
		return nil, err
	}
	return FilterIterator(itr, filter), nil
}

// StreamWithMessageSubstring is the same as Stream, with the exception that entries are filtered for those whose message content contains the substring 'match'. This is functionally equivalent to calling StreamWithFilter using the filter function:
//  func(e Entry) bool {
//      return strings.Contains(e.Msg.Message, match)
//  }
//
// Some implementations of Connection might be able to offload the task of filtering to the underlying storage engine, hence this has the potential to be much more efficient than the corresponding call to StreamWithFilter.
func StreamWithMessageSubstring(ctx context.Context, c Connection, name string, start *Time, finish *Time, match string) (Iterator, error) {
	// Is there anything to do?
	if len(match) == 0 {
		return c.Stream(ctx, name, start, finish)
	}
	// Does the connection support this?
	if d, ok := c.(streamWithMessageSubstringer); ok {
		return d.StreamWithMessageSubstring(ctx, name, start, finish, match)
	} else if d, ok := c.(streamWithMatcheser); ok {
		return d.StreamWithMatches(ctx, name, start, finish, Matches{
			MessageSubstring: match,
		})
	}
	// No luck -- fall back to a filter function
	return StreamWithFilter(ctx, c, name, start, finish, func(e Entry) bool {
		return strings.Contains(e.Msg.Message, match)
	})
}

// StreamWithMessageRegexp is the same as Stream, with the exception that entries are filtered for those whose message content matches the given regular expression. This is functionally equivalent to calling StreamWithFilter using the filter function:
//  re := regexp.MustCompile(expr)
//  func(e Entry) bool {
//      return re.MatchString(e.Msg.Message)
//  }
//
// Some implementations of Connection might be able to offload the task of filtering to the underlying storage engine, hence this has the potential to be much more efficient than the corresponding call to StreamWithFilter.
func StreamWithMessageRegexp(ctx context.Context, c Connection, name string, start *Time, finish *Time, expr string) (Iterator, error) {
	// Does the connection support this?
	if d, ok := c.(streamWithMessageRegexper); ok {
		return d.StreamWithMessageRegexp(ctx, name, start, finish, expr)
	} else if d, ok := c.(streamWithMatcheser); ok {
		return d.StreamWithMatches(ctx, name, start, finish, Matches{
			MessageRegexp: expr,
		})
	}
	// No luck -- compile the regular expression
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}
	// Filter using the regular expression
	return StreamWithFilter(ctx, c, name, start, finish, func(e Entry) bool {
		return re.MatchString(e.Msg.Message)
	})
}

// StreamWithIdentifierSubstring is the same as Stream, with the exception that entries are filtered for those whose message identifier contains the substring 'match'. This is functionally equivalent to calling StreamWithFilter using the filter function:
//  func(e Entry) bool {
//      return strings.Contains(e.Msg.Identifier, match)
//  }
//
// Some implementations of Connection might be able to offload the task of filtering to the underlying storage engine, hence this has the potential to be much more efficient than the corresponding call to StreamWithFilter.
func StreamWithIdentifierSubstring(ctx context.Context, c Connection, name string, start *Time, finish *Time, match string) (Iterator, error) {
	// Is there anything to do?
	if len(match) == 0 {
		return c.Stream(ctx, name, start, finish)
	}
	// Does the connection support this?
	if d, ok := c.(streamWithIdentifierSubstringer); ok {
		return d.StreamWithIdentifierSubstring(ctx, name, start, finish, match)
	} else if d, ok := c.(streamWithMatcheser); ok {
		return d.StreamWithMatches(ctx, name, start, finish, Matches{
			IdentifierSubstring: match,
		})
	}
	// No luck -- fall back to a filter function
	return StreamWithFilter(ctx, c, name, start, finish, func(e Entry) bool {
		return strings.Contains(e.Msg.Identifier, match)
	})
}

// StreamWithIdentifierRegexp is the same as Stream, with the exception that entries are filtered for those whose message identifier matches the given regular expression. This is functionally equivalent to calling StreamWithFilter using the filter function:
//  re := regexp.MustCompile(expr)
//  func(e Entry) bool {
//      return re.MatchString(e.Msg.Identifier)
//  }
//
// Some implementations of Connection might be able to offload the task of filtering to the underlying storage engine, hence this has the potential to be much more efficient than the corresponding call to StreamWithFilter.
func StreamWithIdentifierRegexp(ctx context.Context, c Connection, name string, start *Time, finish *Time, expr string) (Iterator, error) {
	// Does the connection support this?
	if d, ok := c.(streamWithIdentifierRegexper); ok {
		return d.StreamWithIdentifierRegexp(ctx, name, start, finish, expr)
	} else if d, ok := c.(streamWithMatcheser); ok {
		return d.StreamWithMatches(ctx, name, start, finish, Matches{
			IdentifierRegexp: expr,
		})
	}
	// No luck -- compile the regular expression
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}
	// Filter using the regular expression
	return StreamWithFilter(ctx, c, name, start, finish, func(e Entry) bool {
		return re.MatchString(e.Msg.Identifier)
	})
}

// StreamWithMatches is the same as Stream, with the exception that entries are filtered against the conditions 'cond'.
//
// Some implementations of Connection might be able to offload the task of filtering to the underlying storage engine, hence this has the potential to be much more efficient than the corresponding call to StreamWithFilter.
func StreamWithMatches(ctx context.Context, c Connection, name string, start *Time, finish *Time, cond Matches) (Iterator, error) {
	// Does the connection support this?
	if d, ok := c.(streamWithMatcheser); ok {
		return d.StreamWithMatches(ctx, name, start, finish, cond)
	}
	// If we're matching the message against a substring, does c support this?
	if s := cond.MessageSubstring; len(s) != 0 {
		if d, ok := c.(streamWithMessageSubstringer); ok {
			cond.MessageSubstring = ""
			filter, err := buildFilterForMatches(cond)
			if err != nil {
				return nil, err
			}
			itr, err := d.StreamWithMessageSubstring(ctx, name, start, finish, s)
			if err != nil {
				return nil, err
			}
			return FilterIterator(itr, filter), nil
		}
	}
	// If we're matching the message against a regular expression, does c
	// support this?
	if s := cond.MessageRegexp; len(s) != 0 {
		if d, ok := c.(streamWithMessageRegexper); ok {
			cond.MessageRegexp = ""
			filter, err := buildFilterForMatches(cond)
			if err != nil {
				return nil, err
			}
			itr, err := d.StreamWithMessageRegexp(ctx, name, start, finish, s)
			if err != nil {
				return nil, err
			}
			return FilterIterator(itr, filter), nil
		}
	}
	// If we're matching the identifier against a substring, does c support
	// this?
	if s := cond.IdentifierSubstring; len(s) != 0 {
		if d, ok := c.(streamWithIdentifierSubstringer); ok {
			cond.IdentifierSubstring = ""
			filter, err := buildFilterForMatches(cond)
			if err != nil {
				return nil, err
			}
			itr, err := d.StreamWithIdentifierSubstring(ctx, name, start, finish, s)
			if err != nil {
				return nil, err
			}
			return FilterIterator(itr, filter), nil
		}
	}
	// If we're matching the identifier against a regular expression, does c
	// support this?
	if s := cond.IdentifierRegexp; len(s) != 0 {
		if d, ok := c.(streamWithIdentifierRegexper); ok {
			cond.IdentifierRegexp = ""
			filter, err := buildFilterForMatches(cond)
			if err != nil {
				return nil, err
			}
			itr, err := d.StreamWithIdentifierRegexp(ctx, name, start, finish, s)
			if err != nil {
				return nil, err
			}
			return FilterIterator(itr, filter), nil
		}
	}
	// No luck -- we'll have to do all the filtering ourselves
	filter, err := buildFilterForMatches(cond)
	if err != nil {
		return nil, err
	}
	return StreamWithFilter(ctx, c, name, start, finish, filter)
}
