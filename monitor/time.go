// Time defines a data type to represent an instant of time, expressed either as a time or with respect to the start or end of a log.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitor

import (
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/gobutil"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Time represents a point in time, which can be either unassigned, a time, or as an offset from the most recent entry in the log. A positive offset represents an offset into the past; a negative offset represents an offset into the future.
type Time struct {
	isOffset bool
	n        int64
	isTime   bool
	t        time.Time
}

// Encoding constants used when gob-encoding and gob-decoding a Time.
const (
	timeIsUnassigned = iota
	timeIsTime
	timeIsOffset
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// parseExplicit attempts to parse an explicit string into a time. Returns true followed by the time on success. Recognised strings are "today", "tomorrow", and "yesterday".
func parseExplicit(s string, loc *time.Location) (bool, time.Time) {
	// Do we recognise this string?
	switch s {
	case "today":
		n := time.Now().In(loc)
		t := time.Date(n.Year(), n.Month(), n.Day(), 0, 0, 0, 0, loc)
		return true, t
	case "tomorrow":
		n := time.Now().In(loc)
		t := time.Date(n.Year(), n.Month(), n.Day()+1, 0, 0, 0, 0, loc)
		return true, t
	case "yesterday":
		n := time.Now().In(loc)
		t := time.Date(n.Year(), n.Month(), n.Day()-1, 0, 0, 0, 0, loc)
		return true, t
	}
	// No luck
	return false, time.Time{}
}

// parseRelative attempts to parse the given string into a time. Returns true followed by the time on success. Recognised strings are "noon", "midday", and "midnight".
func parseRelative(s string, roundUp bool, loc *time.Location) (bool, time.Time) {
	// Do we recognise this string?
	switch s {
	case "noon", "midday":
		n := time.Now().In(loc)
		t := time.Date(n.Year(), n.Month(), n.Day(), 12, 0, 0, 0, loc)
		if roundUp {
			if !t.After(n) {
				t = time.Date(n.Year(), n.Month(), n.Day()+1, 12, 0, 0, 0, loc)
			}
		} else if !t.Before(n) {
			t = time.Date(n.Year(), n.Month(), n.Day()-1, 12, 0, 0, 0, loc)
		}
		return true, t
	case "midnight":
		n := time.Now().In(loc)
		shift := 0
		if roundUp {
			shift = 1
		}
		t := time.Date(n.Year(), n.Month(), n.Day()+shift, 0, 0, 0, 0, loc)
		return true, t
	}
	// No luck
	return false, time.Time{}
}

// applyDayRounding applies rounding by a day to t.
func applyDayRounding(t time.Time, roundUp bool) time.Time {
	// Zero out anything at resolution hours onwards and round
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	if roundUp {
		t = t.Add(24*time.Hour - 1)
	}
	return t
}

// parseWeekday attempts to parse the given string as a weekday. On success, returns true followed by the time.Weekday. Recognised weekdays are "Monday", "Mon", Tuesday", "Tues", "Tue", etc.
func parseWeekday(s string) (bool, time.Weekday) {
	switch strings.ToLower(s) {
	case "sunday", "sun":
		return true, time.Sunday
	case "monday", "mon":
		return true, time.Monday
	case "tuesday", "tue", "tues":
		return true, time.Tuesday
	case "wednesday", "wed":
		return true, time.Wednesday
	case "thursday", "thu", "thur", "thurs":
		return true, time.Thursday
	case "friday", "fri":
		return true, time.Friday
	case "saturday", "sat":
		return true, time.Saturday
	}
	return false, 0
}

// mostRecentWeekday returns the most recent occurrence of the given weekday.
func mostRecentWeekday(day time.Weekday, loc *time.Location) time.Time {
	// Recover the day
	t := time.Now().In(loc)
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, loc)
	for t.Weekday() != day {
		t = t.AddDate(0, 0, -1)
	}
	return t
}

// parseMonth attempts to parse the given string as a month. On success, returns true followed by the time.Month. Recognised months are "January", "Jan", February", "Feb", etc.
func parseMonth(s string) (bool, time.Month) {
	switch strings.ToLower(s) {
	case "january", "jan":
		return true, time.January
	case "february", "feb":
		return true, time.February
	case "march", "mar":
		return true, time.March
	case "april", "apr":
		return true, time.April
	case "may":
		return true, time.May
	case "june", "jun":
		return true, time.June
	case "july", "jul":
		return true, time.July
	case "august", "aug":
		return true, time.August
	case "september", "sept", "sep":
		return true, time.September
	case "october", "oct":
		return true, time.October
	case "november", "nov":
		return true, time.November
	case "december", "dec":
		return true, time.December
	}
	return false, 0
}

// mostRecentMonth returns the most recent occurrence of the given month.
func mostRecentMonth(month time.Month, loc *time.Location) time.Time {
	// Recover the month
	t := time.Now().In(loc)
	t = time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, loc)
	for t.Month() != month {
		t = t.AddDate(0, -1, 0)
	}
	return t
}

// applyMonthRounding applies rounding by a month to t.
func applyMonthRounding(t time.Time, roundUp bool) time.Time {
	// Zero out anything at resolution days onwards and round
	t = time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
	if roundUp {
		t = time.Date(t.Year(), t.Month()+1, 1, 0, 0, 0, -1, t.Location())
	}
	return t
}

// mostRecentDay takes a time t in year 0, January 1st, such as one returned by parsing a time into time.Kitchen format, and finds the most recent time with that month, day, hour, minute, second, etc.
func mostRecentDay(t time.Time) (time.Time, error) {
	// Sanity check
	if t.Year() != 0 || t.YearDay() != 1 {
		return time.Unix(0, 0), errors.New("time should be in year zero, day 1")
	}
	// Move to the most recent day
	now := time.Now().In(t.Location())
	result := t.AddDate(now.Year(), 0, now.YearDay()-1)
	if result.After(now) {
		result = t.AddDate(now.Year(), 0, now.YearDay()-2)
	}
	return result, nil
}

// mostRecentYear takes a time t in year 0, such as one returned by parsing a time into time.Stamp or time.StampMilli format, and finds the most recent time with that month, day, hour, minute, second, etc.
func mostRecentYear(t time.Time) (time.Time, error) {
	// Sanity check
	if t.Year() != 0 {
		return time.Unix(0, 0), errors.New("time should be in year zero")
	}
	// Move to the most recent year
	now := time.Now().In(t.Location())
	result := t.AddDate(now.Year(), 0, 0)
	if result.After(now) {
		result = t.AddDate(now.Year()-1, 0, 0)
	}
	return result, nil
}

// applyRoundingByFormat applies rounding to t at the resolution defined by the given parse format. Note: If you add additional formats to parseTimeFormat, you will need to add them here too.
func applyRoundingByFormat(t time.Time, format string, roundUp bool) time.Time {
	switch format {
	// Need seconds onwards
	case time.RFC822,
		time.RFC822Z,
		time.Kitchen,
		"Jan _2 15:04",
		"15:04":
		// Zero out anything at resolution seconds onwards and round
		t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, t.Location())
		if roundUp {
			t = t.Add(time.Minute - 1)
		}
	// Need milliseconds onwards
	case time.ANSIC,
		time.UnixDate,
		time.RubyDate,
		time.RFC850,
		time.RFC1123,
		time.RFC1123Z,
		time.Stamp,
		"15:04:05",
		"2006-01-02 15:04:05":
		// Zero out anything at resolution milliseconds onwards and round
		t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), 0, t.Location())
		if roundUp {
			t = t.Add(time.Second - 1)
		}
	// Need microseconds onwards
	case time.StampMilli,
		"15:04:05.000":
		// Zero out anything at resolution microseconds onwards and round
		ms := time.Duration(t.Nanosecond()) / time.Millisecond
		t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), int(ms*time.Millisecond), t.Location())
		if roundUp {
			t = t.Add(time.Millisecond - 1)
		}
	// Need nanoseconds onwards
	case time.StampMicro:
		// Zero out anything at resolution nanoseconds onwards and round
		ms := time.Duration(t.Nanosecond()) / time.Microsecond
		t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), int(ms*time.Microsecond), t.Location())
		if roundUp {
			t = t.Add(time.Microsecond - 1)
		}
	}
	return t
}

// parseTimeFormat attempts to parse the string s into a time using one of a pre-determined list of formats. On success, returns true, the time, and the format used.
func parseTimeFormat(s string, loc *time.Location) (bool, time.Time, string) {
	// Note: if you add additional formats here, you will need to add them to
	// applyRoundingByFormat too.
	// The following formats know the location.
	for _, f := range []string{
		time.UnixDate,
		time.RubyDate,
		time.RFC822,
		time.RFC822Z,
		time.RFC850,
		time.RFC1123,
		time.RFC1123Z,
	} {
		if t, err := time.Parse(f, s); err == nil {
			return true, t.In(loc), f
		}
	}
	// The following formats need the location
	for _, f := range []string{
		time.ANSIC,
		"2006-01-02 15:04:05",
	} {
		if t, err := time.ParseInLocation(f, s, loc); err == nil {
			return true, t, f
		}
	}
	// The following formats need the location and year
	for _, f := range []string{
		time.StampNano,
		time.StampMicro,
		time.StampMilli,
		time.Stamp,
		"Jan _2 15:04",
	} {
		if t, err := time.ParseInLocation(f, s, loc); err == nil {
			if t, err = mostRecentYear(t); err != nil {
				return false, time.Time{}, ""
			}
			return true, t, f
		}
	}
	// The following formats need the location, day, month, and year
	for _, f := range []string{
		time.Kitchen,
		"15:04:05.000",
		"15:04:05",
		"15:04",
	} {
		if t, err := time.ParseInLocation(f, s, loc); err == nil {
			if t, err = mostRecentDay(t); err != nil {
				return false, time.Time{}, ""
			}
			return true, t, f
		}
	}
	// No luck
	return false, time.Time{}, ""
}

//////////////////////////////////////////////////////////////////////
// Time functions
//////////////////////////////////////////////////////////////////////

// IsUnassigned returns true iff t does not have a value assigned (equivalently, if both IsOffset and IsTime return false).
func (t *Time) IsUnassigned() bool {
	return t == nil || (!t.isOffset && !t.isTime)
}

// IsAssigned returns true iff t has a value assigned (equivalently, if either IsOffset or IsTime returns true).
func (t *Time) IsAssigned() bool {
	return t != nil && (t.isOffset || t.isTime)
}

// IsOffset returns true iff t is equal to an offset. If true, also returns the offset amount.
func (t *Time) IsOffset() (bool, int64) {
	if t == nil || !t.isOffset {
		return false, 0
	}
	return true, t.n
}

// IsTime returns true iff t is equal to a time. If true, also returns the time.
func (t *Time) IsTime() (bool, time.Time) {
	if t == nil || !t.isTime {
		return false, time.Time{}
	}
	return true, t.t
}

// Equal returns true iff both t and s are assigned, and one of the following two conditions is satisfied:
// - both t and s represent times, with tTime == sTime;
// - both t and s represent offsets, with tOffset == sOffset.
func (t *Time) Equal(s *Time) bool {
	if ok, t1 := t.IsTime(); ok {
		ok, t2 := s.IsTime()
		return ok && t1.Equal(t2)
	} else if ok, n1 := t.IsOffset(); ok {
		ok, n2 := s.IsOffset()
		return ok && n1 == n2
	}
	return false
}

// After returns true iff both t and s are assigned, and one of the following two conditions is satisfied:
// - both t and s represent times, with tTime.After(sTime);
// - both t and s represent offsets, with tOffset < sOffset.
func (t *Time) After(s *Time) bool {
	if ok, t1 := t.IsTime(); ok {
		ok, t2 := s.IsTime()
		return ok && t1.After(t2)
	} else if ok, n1 := t.IsOffset(); ok {
		ok, n2 := s.IsOffset()
		return ok && n1 < n2
	}
	return false
}

// Before returns true iff both t and s are assigned, and one of the following two conditions is satisfied:
// - both t and s represent times, with tTime < sTime;
// - both t and s represent offsets, with tOffset > sOffset.
func (t *Time) Before(s *Time) bool {
	if ok, t1 := t.IsTime(); ok {
		ok, t2 := s.IsTime()
		return ok && t1.Before(t2)
	} else if ok, n1 := t.IsOffset(); ok {
		ok, n2 := s.IsOffset()
		return ok && n1 > n2
	}
	return false
}

// String returns a string representation of t.
func (t *Time) String() string {
	if ok, tt := t.IsTime(); ok {
		return tt.String()
	} else if ok, n := t.IsOffset(); ok {
		return "offset by " + strconv.FormatInt(n, 10)
	}
	return "unassigned"
}

// MarshalJSON marshals t into JSON.
func (t *Time) MarshalJSON() ([]byte, error) {
	// Fetch a bytes buffer
	b := bytesbuffer.New()
	// Write the opening brace
	b.WriteByte('{')
	// If this is a time or an offset, write the data
	if ok, tt := t.IsTime(); ok {
		b.WriteString("\"time\":")
		s, err := json.Marshal(tt)
		if err != nil {
			return nil, err
		}
		b.Write(s)
	} else if ok, n := t.IsOffset(); ok {
		b.WriteString("\"offset\":")
		b.WriteString(strconv.FormatInt(n, 10))
	}
	// Write the closing brace
	b.WriteByte('}')
	// Make a copy of the bytes, reuse the buffer, and return
	res := make([]byte, b.Len())
	copy(res, b.Bytes())
	bytesbuffer.Reuse(b)
	return res, nil
}

// UnmarshalJSON attempts to unmarshal the given JSON into t.
func (t *Time) UnmarshalJSON(b []byte) error {
	// Sanity check
	if t == nil {
		return errors.New("attempting to unmarshal into a nil Time")
	}
	// Unmarshal into a dummy structure
	temp := &struct {
		Time   []byte
		Offset []byte
	}{}
	if err := json.Unmarshal(b, temp); err != nil {
		return fmt.Errorf("error unmarshalling Time contents: %w", err)
	}
	// Unmarshal the time
	isTime := len(temp.Time) != 0
	var tt time.Time
	if isTime {
		if err := json.Unmarshal(temp.Time, &tt); err != nil {
			return fmt.Errorf("error unmarshalling time: %w", err)
		}
	}
	// Unmarshal the offset
	isOffset := len(temp.Offset) != 0
	var n int64
	if isOffset {
		if err := json.Unmarshal(temp.Offset, &n); err != nil {
			return fmt.Errorf("error unmarshalling offset: %w", err)
		}
	}
	// It cannot be both a time and an offset
	if isTime && isOffset {
		return errors.New("invalid Time (contains both time and offset values)")
	}
	// Looks good, so update the data on t
	t.isTime, t.t = isTime, tt
	t.isOffset, t.n = isOffset, n
	return nil
}

// GobEncode gob-encodes t.
func (t *Time) GobEncode() ([]byte, error) {
	// Fetch a gob-encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// If this is a time or an offset, write the data
	if ok, tt := t.IsTime(); ok {
		if err := enc.Encode(timeIsTime); err != nil {
			return nil, err
		} else if err = enc.Encode(tt); err != nil {
			return nil, err
		}
	} else if ok, n := t.IsOffset(); ok {
		if err := enc.Encode(timeIsOffset); err != nil {
			return nil, err
		} else if err = enc.Encode(n); err != nil {
			return nil, err
		}
	} else if err := enc.Encode(timeIsUnassigned); err != nil {
		return nil, err
	}
	// Return the bytes
	return enc.Bytes(), nil
}

// GobDecode attempts to gob-decode the given bytes into t.
func (t *Time) GobDecode(b []byte) error {
	// Sanity check
	if t == nil {
		return errors.New("attempting to gob-decode into a nil Time")
	}
	// Wrap the byte slice in a gob-decoder
	dec := gobutil.NewDecoder(b)
	// Read the type
	var timeType int
	if err := dec.Decode(&timeType); err != nil {
		return fmt.Errorf("error gob-decoding Time contents: %w", err)
	}
	// How we proceed depends on the type
	var isTime, isOffset bool
	var tt time.Time
	var n int64
	switch timeType {
	case timeIsUnassigned:
	case timeIsTime:
		isTime = true
		if err := dec.Decode(&tt); err != nil {
			return fmt.Errorf("error gob-decoding time: %w", err)
		}
	case timeIsOffset:
		isOffset = true
		if err := dec.Decode(&n); err != nil {
			return fmt.Errorf("error gob-decoding offset: %w", err)
		}
	default:
		return fmt.Errorf("unknown Time contents (type: %d)", timeType)
	}
	// Looks good, so update the data on t
	t.isTime, t.t = isTime, tt
	t.isOffset, t.n = isOffset, n
	return nil
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// NewTime returns a new time set to t.
func NewTime(t time.Time) *Time {
	return &Time{
		isTime: true,
		t:      t,
	}
}

// NewOffset return a new offset set to n.
func NewOffset(n int64) *Time {
	return &Time{
		isOffset: true,
		n:        n,
	}
}

// ParseTime attempts to parse s to a time.Time. Valid possibilities for s are:
//   "now"       - the current time;
//   "today"     - today;
//   "tomorrow"  - tomorrow;
//   "yesterday" - yesterday;
//   "noon", "midday" - noon;
//   "midnight"  - midnight;
//   day of the week - a day of the week ("Monday"/"Mon", "Tuesday"/"Tue",
//                 "Wednesday"/"Wed" etc.), which will be interpreted as the
//                 most recent occurrence of that day;
//   month       - a month ("January"/"Jan", "February"/"Feb" etc.), which
//                 will be interpreted as the most recent occurrence of that
//                 month;
//   a timestamp - valid formats include "3:04PM", "15:04", "15:04:05",
//                 "Jan _2 15:04", "Jan _2 15:04:05", "Jan _2 15:04:05.000",
//				   and "2006-01-02 15:04:05".
// The value of rounding will determine whether the time will be rounded up (towards the end of the day, indicated by 'roundUp' equal to true) or down (towards the start of the day, indicated by 'roundUp' equal to false) when insufficient resolution is given.
func ParseTime(s string, roundUp bool, loc *time.Location) (bool, time.Time) {
	// Sanity check
	s = strings.TrimSpace(s)
	if len(s) == 0 {
		return false, time.Time{}
	}
	// Handle the explicit string cases
	if lower := strings.ToLower(s); lower == "now" {
		return true, time.Now().In(loc)
	} else if ok, t := parseExplicit(lower, loc); ok {
		return true, applyDayRounding(t, roundUp)
	} else if ok, t := parseRelative(lower, roundUp, loc); ok {
		return true, t
	}
	// Handle a weekday
	if ok, day := parseWeekday(s); ok {
		t := mostRecentWeekday(day, loc)
		return true, applyDayRounding(t, roundUp)
	}
	// Handle a month
	if ok, month := parseMonth(s); ok {
		t := mostRecentMonth(month, loc)
		return true, applyMonthRounding(t, roundUp)
	}
	// Handle a formatted timestamp
	if ok, t, f := parseTimeFormat(s, loc); ok {
		return true, applyRoundingByFormat(t, f, roundUp)
	}
	// No luck
	return false, time.Time{}
}

// ParseStartTime attempts to parse s, interpreted as a start time, and return a *Time. Valid possibilities for s are as in ParseTime, along with:
//   "start", "beginning" - the start of the log, represented by an
//                 unassigned time;
//   a duration  - the current time subtract the duration;
//   an integer  - an offset by this value.
func ParseStartTime(s string, loc *time.Location) (bool, *Time) {
	s = strings.TrimSpace(s)
	// Try and parse the string
	if ok, t := ParseTime(s, false, loc); ok {
		return true, NewTime(t)
	}
	// Handle the explicit string case
	switch strings.ToLower(s) {
	case "start", "beginning":
		return true, &Time{}
	}
	// Handle the integer case
	if n, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true, NewOffset(n)
	}
	// Handle a duration
	if d, err := time.ParseDuration(s); err == nil {
		return true, NewTime(time.Now().In(loc).Add(-d))
	}
	// No luck
	return false, nil
}

// ParseEndTime attempts to parse s, interpreted as an end time, and return a *Time. Valid possibilities for s are as in ParseTime, along with:
//   "never", "none" - an unassigned time;
//   "end", "finish" - the last entry in the log, represented by an
//                 offset of 0;
//   a duration  - the current time subtract the duration;
//   an integer  - an offset by this value.
func ParseEndTime(s string, loc *time.Location) (bool, *Time) {
	s = strings.TrimSpace(s)
	// Try and parse the string
	if ok, t := ParseTime(s, true, loc); ok {
		return true, NewTime(t)
	}
	// Handle the explicit string cases
	switch strings.ToLower(s) {
	case "never", "none":
		return true, &Time{}
	case "end", "finish":
		return true, NewOffset(0)
	}
	// Handle the integer case
	if n, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true, NewOffset(n)
	}
	// Handle a duration
	if d, err := time.ParseDuration(s); err == nil {
		return true, NewTime(time.Now().In(loc).Add(-d))
	}
	// No luck
	return false, nil
}
