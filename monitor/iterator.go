// Iterator defines an iterator for an Entry.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitor

import (
	"context"
	"errors"
)

// Iterator describes an Entry iterator.
type Iterator interface {
	// Close closes the iterator, preventing further iteration.
	Close() error
	// Err returns the last error, if any, encountered during iteration. Err
	// may be called after Close.
	Err() error
	// Next advances the iterator. Returns true on successful advance of the
	// iterator; false otherwise. Next or NextContext must be called before the
	// first call to Scan.
	Next() bool
	// NextContext advances the iterator. Returns true on successful advance of
	// the iterator; false otherwise. Next or NextContext must be called before
	// the first call to Scan.
	NextContext(ctx context.Context) (bool, error)
	// Scan copies the current Entry into "dest". Any previously set data in
	// "dest" will be deleted or overwritten.
	Scan(dest *Entry) error
}

// BufferedIterator described an Iterator with buffer.
type BufferedIterator interface {
	Iterator
	// Buffered returns true iff the next call to Next or NextContext is
	// guaranteed not to block.
	Buffered() bool
}

// filterIterator allows filtering of an Iterator's entries.
type filterIterator struct {
	itr    Iterator   // The wrapped iterator
	filter FilterFunc // The filter function
}

// sliceIterator allows iteration over an Entry slice.
type sliceIterator struct {
	s   []Entry // The underlying slice of entries
	idx int     // The current index in the slice
}

// emptyIterator provides an Iterator with no entries.
type emptyIterator struct {
	isClosed bool // Have we closed?
}

// bufferedIterator provides buffering of entries.
type bufferedIterator struct {
	entryC   <-chan *Entry      // The entry channel
	errC     <-chan error       // The error channel
	cancel   context.CancelFunc // The cancel function
	next     *Entry             // The next entry (if known)
	cur      *Entry             // The current entry (if known)
	isClosed bool               // Have we closed?
	err      error              // The error (if any)
}

// Common iteration errors.
var (
	ErrIterationFinished = errors.New("attempting to Scan after iteration has ended")
	ErrNextNotCalled     = errors.New("attempting to Scan before a successful call to Next or NextContext")
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isContextError returns true iff err is a context error.
func isContextError(err error) bool {
	return err == context.Canceled || err == context.DeadlineExceeded
}

// iteratorWorker reads from the given iterator, feeding the results down the entryC channel and returning when either the context fires, the end of iteration is reached, or an error occurs. Any error will be fed down errC. On return the iterator will be closed, and the entryC and errC channels will be closed. Intended to be run in its own go routine.
func iteratorWorker(ctx context.Context, itr Iterator, entryC chan<- *Entry, errC chan<- error) {
	// Defer a graceful shutdown
	var err error
	defer func() {
		// Close the entry channel
		close(entryC)
		// Close the iterator and note any errors during iteration
		if closeErr := itr.Close(); closeErr != nil {
			if err == nil || isContextError(err) {
				err = closeErr
			}
		}
		if itrErr := itr.Err(); itrErr != nil {
			if err == nil || isContextError(err) {
				err = itrErr
			}
		}
		// Pass any error down the error channel and close the channel
		if err != nil {
			errC <- err
		}
		close(errC)
	}()
	// Start reading from the iterator
	var ok bool
	ok, err = itr.NextContext(ctx)
	for err == nil && ok {
		// Read in the next entry
		e := &Entry{}
		if err = itr.Scan(e); err != nil {
			return
		}
		// Feed the entry down the entry channel
		select {
		case entryC <- e:
		case <-ctx.Done():
			err = ctx.Err()
			return
		}
		// Move on
		ok, err = itr.NextContext(ctx)
	}
}

/////////////////////////////////////////////////////////////////////////
// filterIterator functions
/////////////////////////////////////////////////////////////////////////

// FilterIterator wraps the given Iterator, filtering the entries using the given FilterFunc. Calling Close on the returned iterator will Close the wrapped iterator 'itr'.
func FilterIterator(itr Iterator, filter FilterFunc) Iterator {
	if filter == nil {
		return itr
	}
	return &filterIterator{
		itr:    itr,
		filter: filter,
	}
}

// Close closes the iterator, preventing further iteration. Note that this will close the wrapped iterator.
func (itr *filterIterator) Close() error {
	return itr.itr.Close()
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *filterIterator) Err() error {
	return itr.itr.Err()
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *filterIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *filterIterator) NextContext(ctx context.Context) (bool, error) {
	e := Entry{}
	ok, err := itr.itr.NextContext(ctx)
	for err == nil && ok {
		if err = itr.itr.Scan(&e); err != nil {
			return false, err
		} else if execFilterFunc(itr.filter, e) {
			return true, nil
		}
		ok, err = itr.itr.NextContext(ctx)
	}
	return false, err
}

// Scan copies the current Entry into "dest". Any previously set data in "dest" will be deleted or overwritten.
func (itr *filterIterator) Scan(dest *Entry) error {
	return itr.itr.Scan(dest)
}

/////////////////////////////////////////////////////////////////////////
// sliceIterator functions
/////////////////////////////////////////////////////////////////////////

// SliceIterator returns an iterator for the given slice.
func SliceIterator(S []Entry) Iterator {
	return &sliceIterator{
		s:   S,
		idx: -1,
	}
}

// Close prevents future iteration. Always returns nil.
func (itr *sliceIterator) Close() error {
	itr.idx = len(itr.s)
	return nil
}

// Err always returns nil.
func (*sliceIterator) Err() error {
	return nil
}

// Buffered always returns true.
func (*sliceIterator) Buffered() bool {
	return true
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *sliceIterator) Next() bool {
	n := len(itr.s)
	if itr.idx < n {
		itr.idx++
	}
	return itr.idx < n
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *sliceIterator) NextContext(_ context.Context) (bool, error) {
	return itr.Next(), nil
}

// Scan copies the current Entry into "dest". Any previously set data in "dest" will be deleted or overwritten.
func (itr *sliceIterator) Scan(dest *Entry) error {
	if itr.idx == len(itr.s) {
		return ErrIterationFinished
	} else if itr.idx == -1 {
		return ErrNextNotCalled
	}
	Scan(dest, itr.s[itr.idx])
	return nil
}

/////////////////////////////////////////////////////////////////////////
// emptyIterator functions
/////////////////////////////////////////////////////////////////////////

// EmptyIterator returns an iterator with no entries.
func EmptyIterator() Iterator {
	return &emptyIterator{}
}

// Close prevents future iteration. Always returns nil.
func (itr *emptyIterator) Close() error {
	itr.isClosed = true
	return nil
}

// Err always returns nil.
func (*emptyIterator) Err() error {
	return nil
}

// Buffered always returns true.
func (*emptyIterator) Buffered() bool {
	return true
}

// Next advances the iterator. Always returns false.
func (*emptyIterator) Next() bool {
	return false
}

// NextContext advances the iterator. Always returns false.
func (*emptyIterator) NextContext(_ context.Context) (bool, error) {
	return false, nil
}

// Scan will always fail with an error.
func (itr *emptyIterator) Scan(_ *Entry) error {
	if itr.isClosed {
		return ErrIterationFinished
	}
	return ErrNextNotCalled
}

/////////////////////////////////////////////////////////////////////////
// bufferedIterator functions
/////////////////////////////////////////////////////////////////////////

// NewBufferedIterator returns an iterator wrapping 'itr' that buffers the entries. Calling Close on the returned iterator will Close the wrapped iterator 'itr'.
func NewBufferedIterator(itr Iterator, bufSize int) BufferedIterator {
	// Don't buffer a buffer
	if b, ok := itr.(BufferedIterator); ok {
		return b
	}
	// Sanity check
	if bufSize < 0 {
		bufSize = 0
	}
	// Create the communication channels
	entryC := make(chan *Entry, bufSize)
	errC := make(chan error, 1)
	// Create a cancellation context
	ctx, cancel := context.WithCancel(context.Background())
	// Start the background worker
	go iteratorWorker(ctx, itr, entryC, errC)
	// Return the iterator
	return &bufferedIterator{
		entryC: entryC,
		errC:   errC,
		cancel: cancel,
	}
}

// Close prevents future iteration. Note that this will close the wrapped iterator.
func (itr *bufferedIterator) Close() error {
	if !itr.isClosed {
		itr.isClosed = true
		itr.cancel()
		itr.err = <-itr.errC
	}
	return itr.err
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *bufferedIterator) Err() error {
	return itr.err
}

// Buffered returns true iff the next call to Next or NextContext is guaranteed not to block.
func (itr *bufferedIterator) Buffered() bool {
	// Is this easy to answer?
	if itr.isClosed || itr.next != nil {
		return true
	}
	// Try to fetch the next entry without blocking
	select {
	case e, ok := <-itr.entryC:
		if !ok {
			itr.Close()
		} else {
			itr.next = e
		}
		return true
	default:
	}
	return false
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *bufferedIterator) Next() bool {
	// Sanity check
	if itr.isClosed {
		return false
	}
	// Is the next entry already cached?
	if itr.next != nil {
		itr.cur, itr.next = itr.next, nil
		return true
	}
	// Fetch the entry
	e, ok := <-itr.entryC
	if !ok {
		itr.Close()
		return false
	}
	itr.cur = e
	return true
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *bufferedIterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if itr.isClosed {
		return false, nil
	}
	// Is the next entry already cached?
	if itr.next != nil {
		itr.cur, itr.next = itr.next, nil
		return true, nil
	}
	// Fetch the entry
	select {
	case e, ok := <-itr.entryC:
		if !ok {
			itr.Close()
			return false, nil
		}
		itr.cur = e
	case <-ctx.Done():
		return false, ctx.Err()
	}
	return true, nil
}

// Scan copies the current Entry into "dest". Any previously set data in "dest" will be deleted or overwritten.
func (itr *bufferedIterator) Scan(dest *Entry) error {
	if itr.isClosed {
		return ErrIterationFinished
	} else if itr.cur == nil {
		return ErrNextNotCalled
	}
	Scan(dest, *itr.cur)
	return nil
}
