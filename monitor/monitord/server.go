// Server implements a monitord server

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitord

import (
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/logger/monitor/monitord/internal/monitordrpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/protobuf/types/known/wrapperspb"

	// Make our compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that monitord listens on.
const (
	DefaultTCPPort = 12357
	DefaultWSPort  = 80
)

// monitorServer implements the tasks on the gRPC server.
type monitorServer struct {
	monitordrpc.UnimplementedMonitorServer
	log.BasicLogable
	c monitor.Connection // The connection to the log source
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isContextError returns true iff err is a context error.
func isContextError(err error) bool {
	return err == context.Canceled || err == context.DeadlineExceeded
}

// writeIteratorToStream reads from the iterator, writing the entries to the stream s. It is the caller's responsibility to close the iterator and check for iteration errors.
func writeIteratorToStream(ctx context.Context, s monitordrpc.Monitor_StreamServer, itr monitor.Iterator) error {
	e := monitor.Entry{}
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		if err := itr.Scan(&e); err != nil {
			return err
		} else if x, err := monitordrpc.FromEntry(e); err != nil {
			return err
		} else if err := s.Send(x); err != nil {
			return err
		}
		ok, err = itr.NextContext(ctx)
	}
	return err
}

/////////////////////////////////////////////////////////////////////////
// monitorServer functions
/////////////////////////////////////////////////////////////////////////

// IsLogName returns true iff the given string is a log name.
func (s *monitorServer) IsLogName(ctx context.Context, l *monitordrpc.LogName) (*wrapperspb.BoolValue, error) {
	ok, err := monitor.IsLogName(ctx, s.c, l.GetName())
	if err != nil {
		return nil, err
	}
	return wrapperspb.Bool(ok), nil
}

// ListLogNames writes out the log names to 'stream'. Log names are filtered against the conditions 'cond'.
func (s *monitorServer) ListLogNames(cond *monitordrpc.NameMatches, stream monitordrpc.Monitor_ListLogNamesServer) error {
	// Fetch the log names
	S, err := monitor.LogNamesWithMatches(stream.Context(), s.c, monitordrpc.FromNameMatches(cond))
	if err != nil {
		return err
	}
	// Write the log names to the stream
	for _, n := range S {
		if err := stream.Send(monitordrpc.ToLogName(n)); err != nil {
			return err
		}
	}
	return nil
}

// Stream writes a stream of entries to the client. Entries are filtered against the conditions described by 'cond'.
func (s *monitorServer) Stream(cond *monitordrpc.StreamMatches, stream monitordrpc.Monitor_StreamServer) (err error) {
	// We defer clearing any context errors
	defer func() {
		if isContextError(err) {
			err = nil
		}
	}()
	// Unpack and validate the 'cond'
	var name string
	var start, finish *monitor.Time
	var match monitor.Matches
	if name, start, finish, match, err = monitordrpc.FromStreamMatches(cond); err != nil {
		return
	}
	// Fetch the results
	var itr monitor.Iterator
	if itr, err = monitor.StreamWithMatches(stream.Context(), s.c, name, start, finish, match); err != nil {
		return
	}
	// Defer closing the iterator and checking for iteration errors
	defer func() {
		if closeErr := itr.Close(); closeErr != nil {
			if err == nil || isContextError(err) {
				err = closeErr
			}
		}
		if itrErr := itr.Err(); itrErr != nil {
			if err == nil || isContextError(err) {
				err = itrErr
			}
		}
	}()
	// Write the results to the stream
	err = writeIteratorToStream(stream.Context(), stream, itr)
	return
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new monitord server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &monitorServer{c: opts.Connection}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerErrorInterceptor,
			grpclog.StreamServerInterceptor(m.Log()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the underlying gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	monitordrpc.RegisterMonitorServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer: m,
		Server:      s,
	}, nil
}
