// Client implements a monitord client that satisfies monitor.Connection

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitord

import (
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/logger/monitor/monitord/internal/monitordrpc"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"net/url"
	"strconv"
)

// Client is the client-view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client monitordrpc.MonitorClient // The gRPC client
}

// Assert that a Client satisfies monitor.Connection.
var _ = monitor.Connection(&Client{})

// streamIterator implements the montior.Iterator interface.
type streamIterator struct {
	stream     monitordrpc.Monitor_StreamClient // The underlying stream
	cancel     context.CancelFunc               // Cancel the stream context
	calledNext bool                             // Has Next been called?
	e          monitor.Entry                    // The next entry
	isClosed   bool                             // Have we closed?
	err        error                            // The error during iteration
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

//////////////////////////////////////////////////////////////////////
// streamIterator functions
//////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration.
func (itr *streamIterator) Close() error {
	if !itr.isClosed {
		itr.isClosed = true
		itr.cancel()
	}
	return itr.err
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *streamIterator) Err() error {
	return itr.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *streamIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *streamIterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if itr.isClosed || itr.err != nil {
		return false, nil
	}
	itr.calledNext = true
	// Link the user-provided context to our cancel function
	defer contextutil.NewLink(ctx, itr.cancel).Stop()
	// Read in the next rpc entry from the stream
	x, err := itr.stream.Recv()
	if err != nil {
		if err == io.EOF {
			err = metadataToError(itr.stream.Trailer())
		}
		itr.err = grpcutil.ConvertError(err)
		return false, itr.err
	}
	// Convert the rpc entry
	if err := monitordrpc.Scan(&itr.e, x); err != nil {
		itr.err = err
		return false, err
	}
	return true, nil
}

// Scan copies the current Entry into "dest". Any previously set data in "dest" will be deleted or overwritten.
func (itr *streamIterator) Scan(dest *monitor.Entry) error {
	if itr.err != nil {
		return itr.err
	} else if itr.isClosed {
		return monitor.ErrIterationFinished
	} else if !itr.calledNext {
		return monitor.ErrNextNotCalled
	}
	monitor.Scan(dest, itr.e)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client-view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity check
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
			grpclog.UnaryClientInterceptor(c.Log()),
		),
		grpc.WithChainStreamInterceptor(
			grpclog.StreamClientInterceptor(c.Log()),
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = monitordrpc.NewMonitorClient(conn)
	return c, nil
}

// IsLogName returns true iff the given string is a log name.
func (c *Client) IsLogName(ctx context.Context, name string) (bool, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return false, errors.New("uninitialised client")
	}
	// Submit the message to the server
	b, err := c.client.IsLogName(ctx, monitordrpc.ToLogName(name))
	if err != nil {
		return false, err
	}
	return b.GetValue(), nil
}

// LogNames returns the log names.
func (c *Client) LogNames(ctx context.Context) ([]string, error) {
	return c.LogNamesWithMatches(ctx, monitor.NameMatches{})
}

// LogNamesWithMatches is the same as LogNames, with the exception that log names are filtered against the conditions 'cond'.
func (c *Client) LogNamesWithMatches(ctx context.Context, cond monitor.NameMatches) ([]string, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Submit the message to the server
	stream, err := c.client.ListLogNames(ctx, monitordrpc.ToNameMatches(cond))
	if err != nil {
		return nil, grpcutil.ConvertError(err)
	}
	// Extract the names
	results := make([]string, 0, 10)
	for {
		// Read a log name from the stream
		l, err := stream.Recv()
		if err != nil {
			break
		}
		results = append(results, l.GetName())
	}
	// Check that nothing went wrong
	if err == io.EOF {
		err = metadataToError(stream.Trailer())
	}
	return results, grpcutil.ConvertError(err)
}

// Stream returns an iterator down which entries from the log 'name' are passed. Only entries falling into the range determined by start and finish will be returned. If start is unassigned then the start of the log will be used; if end is unassigned then no end is used. The caller should ensure that the returned Iterator is closed, otherwise a resource leak will result.
func (c *Client) Stream(ctx context.Context, name string, start *monitor.Time, finish *monitor.Time) (monitor.Iterator, error) {
	return c.StreamWithMatches(ctx, name, start, finish, monitor.Matches{})
}

// StreamWithMatches is the same as Stream, with the exception that entries are filtered against the conditions 'cond'.
func (c *Client) StreamWithMatches(ctx context.Context, name string, start *monitor.Time, finish *monitor.Time, cond monitor.Matches) (monitor.Iterator, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("uninitialised client")
	}
	// Convert the arguments to rpc format
	m, err := monitordrpc.ToStreamMatches(name, start, finish, cond)
	if err != nil {
		return nil, err
	}
	// Construct a new context that will last the entire iteration and link it
	// to the user's context for the duration of this call
	newCtx, cancel := context.WithCancel(context.Background())
	defer contextutil.NewLink(ctx, cancel).Stop()
	// Send the stream request
	stream, err := c.client.Stream(newCtx, m)
	if err != nil {
		cancel()
		return nil, grpcutil.ConvertError(err)
	}
	// Wrap the stream up as an iterator
	return &streamIterator{
		stream: stream,
		cancel: cancel,
	}, nil
}
