// Monitordflag.go provides a standard set of command line flags that set the default values for the monitor client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitordflag

import (
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"errors"
)

// Set represents a set of command-line flags defined by a client config.
type Set struct {
	c    *monitord.ClientConfig // The client config
	addr flag.Flag              // The address flag
}

/////////////////////////////////////////////////////////////////////////
// Set functions
/////////////////////////////////////////////////////////////////////////

// NewSet returns a set of command-line flags with defaults given by c. If c is nil then the monitord.DefaultConfig() will be used. Note that this does not update the default client config, nor does this update c. To recover the updated client config after parse, call the ClientConfig() method on the returned set.
func NewSet(c *monitord.ClientConfig) *Set {
	// Either move to the default client config, or move to a copy
	if c == nil {
		c = monitord.DefaultConfig()
	} else {
		c = c.Copy()
	}
	// Create the address flags
	addr := address.NewFlag(
		"monitor-address",
		&c.Address, c.Address,
		"The address of the pcas monitor server",
		address.EnvUsage("monitor-address", "PCAS_MONITOR_ADDRESS"),
	)
	// Return the set
	return &Set{
		c:    c,
		addr: addr,
	}
}

// Flags returns the members of the set.
func (s *Set) Flags() []flag.Flag {
	if s == nil {
		return nil
	}
	return []flag.Flag{s.addr}
}

// Name returns the name of this collection of flags.
func (*Set) Name() string {
	return "Monitor options"
}

// UsageFooter returns the footer for the usage message for this flag set.
func (*Set) UsageFooter() string {
	return ""
}

// UsageHeader returns the header for the usage message for this flag set.
func (*Set) UsageHeader() string {
	return ""
}

// Validate validates the flag set.
func (s *Set) Validate() error {
	if s == nil {
		return errors.New("flag set is nil")
	}
	return s.c.Validate()
}

// ClientConfig returns the client config described by this set. This should only be called after the set has been successfully validated.
func (s *Set) ClientConfig() *monitord.ClientConfig {
	if s == nil {
		return monitord.DefaultConfig()
	}
	return s.c.Copy()
}
