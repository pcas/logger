//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. monitord.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitordrpc

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/monitor"
	"errors"
	"google.golang.org/protobuf/types/known/timestamppb"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// ToTime return returns the monitor.Time object corresponding to the Time object t.
func ToTime(t *Time) (*monitor.Time, error) {
	switch val := t.GetType().(type) {
	case *Time_Unassigned:
		return &monitor.Time{}, nil
	case *Time_Offset:
		return monitor.NewOffset(val.Offset), nil
	case *Time_Time:
		return monitor.NewTime(val.Time.AsTime()), nil
	}
	return nil, errors.New("malformed time")
}

// FromTime returns the Time object corresponding to the monitor.Time object t.
func FromTime(t *monitor.Time) (*Time, error) {
	if t.IsUnassigned() {
		return &Time{
			Type: &Time_Unassigned{
				Unassigned: true,
			},
		}, nil
	} else if ok, n := t.IsOffset(); ok {
		return &Time{
			Type: &Time_Offset{
				Offset: n,
			},
		}, nil
	} else if ok, thisT := t.IsTime(); ok {
		return &Time{
			Type: &Time_Time{
				Time: timestamppb.New(thisT),
			},
		}, nil
	}
	return nil, errors.New("malformed time")
}

// Scan converts the Entry e, scanning the result into the monitor.Entry object "dest". Any previously set data in "dest" will be deleted or overwritten.
func Scan(dest *monitor.Entry, e *Entry) error {
	// Attempt to convert the data field in e (if present) from JSON
	var data interface{}
	if s := e.GetData(); len(s) != 0 {
		data = logger.UnmarshalJSONData(s)
	}
	// Set the values on dest and returns
	dest.T = e.GetTime().AsTime()
	dest.Msg.Identifier = e.GetIdentifier()
	dest.Msg.LogName = e.GetLogName()
	dest.Msg.Message = e.GetLogMessage()
	dest.Msg.Data = data
	return nil
}

// FromEntry returns the Entry corresponding to the monitor.Entry object e.
func FromEntry(e monitor.Entry) (*Entry, error) {
	// Attempt to marshal the data field in e to JSON
	var src []byte
	if e.Msg.Data != nil {
		src = logger.MarshalJSONData(e.Msg.Data)
	}
	// Wrap up the rest of the information and return
	return &Entry{
		Identifier: e.Msg.Identifier,
		LogName:    e.Msg.LogName,
		LogMessage: e.Msg.Message,
		Data:       string(src),
		Time:       timestamppb.New(e.T),
	}, nil
}

// ToLogName returns the LogName object corresponding to the string n
func ToLogName(n string) *LogName {
	return &LogName{Name: n}
}

// ToNameMatches returns the NameMatches object corresponding to the monitor.NameMatches object n
func ToNameMatches(n monitor.NameMatches) *NameMatches {
	return &NameMatches{
		Substring:    n.Substring,
		NotSubstring: n.NotSubstring,
		Regexp:       n.Regexp,
		NotRegexp:    n.NotRegexp,
	}
}

// FromNameMatches returns the monitor.NameMatches object corresponding to the NameMatches object n
func FromNameMatches(n *NameMatches) monitor.NameMatches {
	return monitor.NameMatches{
		Substring:    n.Substring,
		NotSubstring: n.NotSubstring,
		Regexp:       n.Regexp,
		NotRegexp:    n.NotRegexp,
	}
}

// ToStreamMatches returns the StreamMatches object corresponding to the given log name, start time,finish time, and the monitor.Matches object m
func ToStreamMatches(name string, start *monitor.Time, finish *monitor.Time, m monitor.Matches) (*StreamMatches, error) {
	// Wrap up the start and finish times
	startSpec, err := FromTime(start)
	if err != nil {
		return nil, err
	}
	finishSpec, err := FromTime(finish)
	if err != nil {
		return nil, err
	}
	return &StreamMatches{
		LogName: ToLogName(name),
		From:    startSpec,
		To:      finishSpec,
		Match: &EntryMatches{
			MessageSubstring:       m.MessageSubstring,
			MessageNotSubstring:    m.MessageNotSubstring,
			MessageRegexp:          m.MessageRegexp,
			MessageNotRegexp:       m.MessageNotRegexp,
			IdentifierSubstring:    m.IdentifierSubstring,
			IdentifierNotSubstring: m.IdentifierNotSubstring,
			IdentifierRegexp:       m.IdentifierRegexp,
			IdentifierNotRegexp:    m.IdentifierNotRegexp,
		},
	}, nil
}

// FromStreamMatches returns the log name, start time, finish time, and the monitor.Matches object match corresponding to the StreamMatches object m
func FromStreamMatches(m *StreamMatches) (name string, start *monitor.Time, finish *monitor.Time, match monitor.Matches, err error) {
	// grab the log name
	name = m.LogName.GetName()
	// unwrap the start and end times
	start, err = ToTime(m.From)
	if err != nil {
		return "", nil, nil, monitor.Matches{}, err
	}
	finish, err = ToTime(m.To)
	if err != nil {
		return "", nil, nil, monitor.Matches{}, err
	}
	match = monitor.Matches{
		MessageSubstring:       m.Match.MessageSubstring,
		MessageNotSubstring:    m.Match.MessageNotSubstring,
		MessageRegexp:          m.Match.MessageRegexp,
		MessageNotRegexp:       m.Match.MessageNotRegexp,
		IdentifierSubstring:    m.Match.IdentifierSubstring,
		IdentifierNotSubstring: m.Match.IdentifierNotSubstring,
		IdentifierRegexp:       m.Match.IdentifierRegexp,
		IdentifierNotRegexp:    m.Match.IdentifierNotRegexp,
	}
	return
}
