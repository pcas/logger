// Clientoptions specifies the options that can be set for the monitord client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitord

import (
	"bitbucket.org/pcastools/address"
	"errors"
	"os"
	"strconv"
	"sync"
)

// Common errors
var (
	ErrNilConfiguration = errors.New("illegal nil configuration data")
	ErrPortOutOfRange   = errors.New("the port number must be between 1 and 65535")
)

// ClientConfig describes the configuration options we allow a user to set on a client connection.
type ClientConfig struct {
	Address     *address.Address // The address to connect to
	SSLDisabled bool             // Disable SSL?
	SSLCert     []byte           // The SSL certificate (if any)
}

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *ClientConfig
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	// Create the default address
	addr, err := address.NewTCP("localhost", DefaultTCPPort)
	if err != nil {
		panic(err) // This should never happen
	}
	// Create the initial defaults
	defaults = &ClientConfig{
		Address: addr,
	}
	// Modify the defaults based on environment variables
	if val, ok := os.LookupEnv("PCAS_MONITOR_ADDRESS"); ok {
		addr, err := address.New(val)
		if err == nil {
			defaults.Address = addr
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// ClientConfig functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
//
// The initial default value for the host and port will be read from the environment variable
//	PCAS_MONITOR_ADDRESS = "hostname[:port]" or "ws://host/path"
// on package init.
func DefaultConfig() *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default client configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *ClientConfig) *ClientConfig {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the client configuration, returning an error if there's a problem.
func (c *ClientConfig) Validate() error {
	if c == nil || c.Address == nil {
		return ErrNilConfiguration
	}
	return nil
}

// URI returns the URI connection string.
//
// Note that the returned string may differ slightly from the value returned c.Address.URI(). For example, if c.Address omits a port number, an appropriate default port number is used here.
func (c *ClientConfig) URI() string {
	a := c.Address
	var port int
	switch a.Scheme() {
	case "ws":
		port = DefaultWSPort
		if a.HasPort() {
			port = a.Port()
		}
	default:
		port = DefaultTCPPort
		if a.HasPort() {
			port = a.Port()
		}
	}
	return a.Scheme() + "://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
}

// Copy returns a copy of the configuration.
func (c *ClientConfig) Copy() *ClientConfig {
	cc := *c
	// Make a copy of the SSLCert
	if c.SSLCert != nil {
		crt := make([]byte, len(c.SSLCert))
		copy(crt, c.SSLCert)
		cc.SSLCert = crt
	}
	return &cc
}
