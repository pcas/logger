// Errors contains code for transmitting errors back from the server in the trailer metadata.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitord

import (
	"bitbucket.org/pcastools/grpcutil"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"strconv"
)

// serverError encodes an error from the server.
type serverError struct {
	msg   string // The error message
	cause error  // The cause (if any)
}

/////////////////////////////////////////////////////////////////////////
// serverError functions
/////////////////////////////////////////////////////////////////////////

// Error returns a description of the error.
func (e *serverError) Error() string {
	msg := e.msg
	if cause := e.Cause(); cause != nil {
		msg += ": " + cause.Error()
	}
	return msg
}

// Cause returns the underlying cause of the error, if known.
func (e *serverError) Cause() error {
	return e.cause
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// errorToMetadata encodes details about the given error as metadata.
func errorToMetadata(err error) metadata.MD {
	md := make(map[string]string)
	// Recursively add information about this error and its cause
	type causer interface {
		Cause() error
	}
	var depth int
	for err != nil {
		// Add the data about the error
		key := "error"
		if depth != 0 {
			key += "_cause_" + strconv.Itoa(depth)
		}
		md[key] = err.Error()
		// Move on to the cause, if known
		if e, ok := err.(causer); ok {
			err = e.Cause()
		} else {
			err = nil
		}
		depth++
	}
	// Return the metadata
	return metadata.New(md)
}

// extractCauseFromMetadata extracts the cause from the given metadata.
func extractCauseFromMetadata(md metadata.MD) (cause error) {
	// Scan through the metadata to find the number of causes
	depth := 0
	var done bool
	for !done {
		if len(md.Get("error_cause_"+strconv.Itoa(depth+1))) == 0 {
			done = true
		} else {
			depth++
		}
	}
	// Build the cause
	for ; depth > 0; depth-- {
		cause = &serverError{
			msg:   md.Get("error_cause_" + strconv.Itoa(depth))[0],
			cause: cause,
		}
	}
	return
}

// metadataToError decodes details about an error from the given metadata.
func metadataToError(md metadata.MD) error {
	S := md.Get("error")
	if len(S) == 0 {
		return nil
	}
	return &serverError{
		msg:   S[0],
		cause: extractCauseFromMetadata(md),
	}
}

// unaryServerErrorInterceptor is a unary server interceptor for encoding errors in the trailer metadata.
func unaryServerErrorInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in %s: %v", info.FullMethod, e)
		}
	}()
	resp, err = handler(ctx, req)
	if err != nil {
		err = grpc.SetTrailer(ctx, errorToMetadata(err))
	}
	return
}

// streamServerErrorInterceptor is a stream server interceptor for encoding errors in the trailer metadata.
func streamServerErrorInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in %s: %v", info.FullMethod, e)
		}
	}()
	err = handler(srv, stream)
	if err != nil {
		stream.SetTrailer(errorToMetadata(err))
		err = nil
	}
	return
}

// unaryClientErrorInterceptor is a unary client interceptor for extracting errors in the trailer metadata.
func unaryClientErrorInterceptor(ctx context.Context, method string, req interface{}, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	var trailer metadata.MD
	opts = append(opts, grpc.Trailer(&trailer))
	err := invoker(ctx, method, req, reply, cc, opts...)
	if err == nil {
		err = metadataToError(trailer)
	}
	return grpcutil.ConvertError(err)
}
