// Topic handles recovery of topic data from Kafka.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"time"
)

// topicIterator is an iterator for a topic.
type topicIterator struct {
	log.BasicLogable
	msgC          <-chan *sarama.ConsumerMessage // The message channel
	filter        filterFunc                     // The filter function
	done          doneFunc                       // The done function
	formatter     logger.Formatter               // The formatter
	m             *sarama.ConsumerMessage        // The next message
	deferredClose bool                           // Defer closing
	isClosed      bool                           // Have we closed?
	closeFunc     func() error                   // A close function
	err           error                          // Any error during iteration
}

// doneTimeout is the time-out used by consumeTopic before checking with the done function using a nil-valued message. More precisely, if no messages have been received within this time-out period then the done function will be called with argument nil. If the done function returns true then the consumer will exit.
const doneTimeout = 3 * time.Second

/////////////////////////////////////////////////////////////////////////
// topicIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration.
func (itr *topicIterator) Close() error {
	if !itr.isClosed {
		itr.isClosed = true
		if err := itr.closeFunc(); err != nil {
			itr.Log().Printf("%s", err)
			if itr.err == nil {
				itr.err = err
			}
		}
	}
	return itr.err
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *topicIterator) Err() error {
	return itr.err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *topicIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *topicIterator) NextContext(ctx context.Context) (bool, error) {
	// Sanity check
	if itr.isClosed || itr.err != nil {
		return false, nil
	} else if itr.deferredClose {
		itr.Close()
		return false, nil
	}
	// Create a time-out timer
	t := time.NewTimer(doneTimeout)
	// Consume the partition
	for {
		select {
		case <-t.C:
			if itr.done != nil && itr.done(nil) {
				itr.Close()
				return false, nil
			}
		case <-ctx.Done():
			t.Stop()
			itr.Close()
			return false, ctx.Err()
		case m, ok := <-itr.msgC:
			t.Stop()
			if !ok {
				itr.Close()
				return false, nil
			}
			keep := itr.filter == nil || itr.filter(m)
			if itr.done != nil && itr.done(m) {
				if keep {
					itr.deferredClose = true
				} else {
					itr.Close()
					return false, nil
				}
			}
			if keep {
				itr.m = m
				return true, nil
			}
		}
		// Reset the timer
		t.Reset(doneTimeout)
	}
}

// Scan copies the current Entry into "dest". Any previously set data in "dest" will be deleted or overwritten.
func (itr *topicIterator) Scan(dest *monitor.Entry) error {
	// Sanity check
	if itr.isClosed {
		return monitor.ErrIterationFinished
	} else if itr.m == nil {
		return monitor.ErrNextNotCalled
	}
	// Convert the message using the formatter
	if err := itr.formatter.Scan(&dest.Msg, string(itr.m.Value)); err != nil {
		return fmt.Errorf("unable to parse message: %w", err)
	}
	// Ensure that the log name is set
	if len(dest.Msg.LogName) == 0 {
		dest.Msg.LogName = itr.m.Topic
	}
	// Set the timestamp
	dest.T = itr.m.Timestamp
	return nil
}
