// Done implements functions for working with done functions.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger/monitor"
	"github.com/Shopify/sarama"
	"time"
)

// doneFunc is used by consumeTopic to determine whether consumption of a topic should stop. The argument passed to the done function will be either the current message, or nil. The function should return true iff consumption should terminate.
//
// Note that the done function is called by consumeTopic with a non-nil message *after* that message has been seen by the filter function (and, if appropriate, fed down the message channel to the user). Thus it is safe -- in fact, desirable -- for the done function to return true if it can determine that this is the last valid message in the stream.
type doneFunc func(msg *sarama.ConsumerMessage) bool

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// alwaysDone returns a done function that always returns true.
func alwaysDone() doneFunc {
	return func(_ *sarama.ConsumerMessage) bool {
		return true
	}
}

// doneAfterTime returns a done function so that any time > t returns true.
func doneAfterTime(t time.Time) doneFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		if msg == nil {
			return time.Now().After(t)
		}
		return msg.Timestamp.After(t)
	}
}

// doneOnN returns a done function that returns true after receiving >= n non-nil messages. Assumes that n is a non-negative integer.
func doneOnN(n int64) doneFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		if n > 0 && msg != nil {
			n--
		}
		return n == 0
	}
}

// doneAtOffset returns a done function that returns true if the given message has offset >= n.
func doneAtOffset(n int64) doneFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		return msg != nil && msg.Offset+1 >= n
	}
}

// orDone returns a done function equal to d1 || d2.
func orDone(d1 doneFunc, d2 doneFunc) doneFunc {
	if d1 == nil {
		return d2
	} else if d2 == nil {
		return d1
	}
	return func(msg *sarama.ConsumerMessage) bool {
		return d1(msg) || d2(msg)
	}
}

// createDoneFunc returns a finish function for the given time t. Here 'first' and 'next' are the offsets corresponding to sarama.OffsetOldest and sarama.OffsetNewest, respectively.
func createDoneFunc(k sarama.Client, topic string, first int64, next int64, t *monitor.Time) (doneFunc, error) {
	// Get the unassigned case out of the way
	if t.IsUnassigned() {
		return nil, nil
	}
	// Note the time or offset
	isTime, tt := t.IsTime()
	_, n := t.IsOffset()
	// Handle the case where t is a time strictly in the future
	if isTime && tt.After(time.Now()) {
		return doneAfterTime(tt), nil
	}
	// Handle the case where the log is empty
	if first == next {
		if isTime || n >= 0 {
			return alwaysDone(), nil
		}
		return doneOnN(negate(n)), nil
	}
	// Handle the case where t is a time (necessarily strictly in the past)
	if isTime {
		// Sarama uses milliseconds since Jan 1 1970 UTC as the time format
		n, err := k.GetOffset(topic, partitionID, toKafkaTime(tt))
		if err != nil {
			return nil, err
		} else if n == first {
			return alwaysDone(), nil
		} else if n == sarama.OffsetNewest {
			n = next
		}
		return orDone(doneAtOffset(n), doneAfterTime(tt)), nil
	}
	// Handle the offset
	n = next - n
	if n <= first {
		return alwaysDone(), nil
	}
	return doneAtOffset(n), nil
}
