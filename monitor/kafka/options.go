// Options describes the various options for a Kafka connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger"
	"strings"
	"time"
)

// Option sets options on a Kafka logger.
type Option interface {
	apply(*kafkaOptions)
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*kafkaOptions)
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *kafkaOptions) {
	h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*kafkaOptions)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// kafkaOptions are the options on a Metrics instance.
type kafkaOptions struct {
	Formatter   logger.Formatter // The formatter for the log messages
	BrokerAddrs []string         // The broker addresses for the Kafka consumers
	Lifetime    time.Duration    // The lifetime of a Kafka connection in the pool
}

// The default option values.
const (
	DefaultBrokerAddr = "localhost:9092" // The default broker address to use if no broker addresses are specified.
	DefaultLifetime   = time.Minute
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isInSlice returns true if and only if s is contained in the slice S.
func isInSlice(s string, S []string) bool {
	for _, t := range S {
		if s == t {
			return true
		}
	}
	return false
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) *kafkaOptions {
	// Create the default options
	opts := &kafkaOptions{
		Formatter:   logger.DefaultFormat,
		BrokerAddrs: make([]string, 0, 1),
		Lifetime:    DefaultLifetime,
	}
	// Set the options
	for _, h := range options {
		h.apply(opts)
	}
	// If no brokers have been specified, use the default broker address
	if len(opts.BrokerAddrs) == 0 {
		opts.BrokerAddrs = append(opts.BrokerAddrs, DefaultBrokerAddr)
	}
	return opts
}

// Formatter sets the formatter to use when converting strings to log messages.
func Formatter(f logger.Formatter) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if f == nil {
			f = logger.DefaultFormat
		}
		opts.Formatter = f
	})
}

// AddBrokerAddr adds the given addresses to the slice of Kafka broker address. Broker addresses should be in the format "hostname:port". DefaultBrokerAddr if and only if no broker addresses are added.
func AddBrokerAddr(addr ...string) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		for _, s := range addr {
			s = strings.TrimSpace(s)
			if len(s) > 0 && !isInSlice(s, opts.BrokerAddrs) {
				opts.BrokerAddrs = append(opts.BrokerAddrs, s)
			}
		}
	})
}

// Lifetime sets the lifetime of a Kafka client connection in the connection pool.
func Lifetime(d time.Duration) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		opts.Lifetime = d
	})
}
