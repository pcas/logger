// Filter implements functions for working with filter functions.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger/monitor"
	"github.com/Shopify/sarama"
	"math"
	"time"
)

// filterFunc is used to filter the messages being consumed. The current message is passed to the function; the function should return true iff the message should be passed along to the user.
type filterFunc func(msg *sarama.ConsumerMessage) bool

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// negate returns -n (or close enough).
func negate(n int64) int64 {
	if n == math.MinInt64 {
		return math.MaxInt64
	}
	return -n
}

// toKafkaTime returns the given time in the format expected by Kafka.
func toKafkaTime(t time.Time) int64 {
	return t.In(time.UTC).UnixNano() / int64(time.Millisecond)
}

// filterBeforeTime filters any messages with timestamp < t.
func filterBeforeTime(t time.Time) filterFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		return !msg.Timestamp.Before(t)
	}
}

// filterAfterTime filters any messages with timestamp > t.
func filterAfterTime(t time.Time) filterFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		return !msg.Timestamp.After(t)
	}
}

// filterFirstN filters the first n messages. Assumes that n is a non-negative integer.
func filterFirstN(n int64) filterFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		if n > 0 {
			n--
		}
		return n <= 0
	}
}

// filterAfterN only allows the first n messages through the filter. Assumes that n is a non-negative integer.
func filterAfterN(n int64) filterFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		if n >= 0 {
			n--
		}
		return n >= 0
	}
}

// filterAfterOffset filters any messages with offset > n.
func filterAfterOffset(n int64) filterFunc {
	return func(msg *sarama.ConsumerMessage) bool {
		return msg.Offset <= n
	}
}

// andFilter returns a filter function equal to f1 && f2.
func andFilter(f1 filterFunc, f2 filterFunc) filterFunc {
	if f1 == nil {
		return f2
	} else if f2 == nil {
		return f1
	}
	return func(msg *sarama.ConsumerMessage) bool {
		ok := f2 == nil || f2(msg) // Filters can have side-effects
		return (f1 == nil || f1(msg)) && ok
	}
}

// getStartOffset computes the start offset for the sarama Client k on the given topic at given time t and returns a corresponding filter function. Here 'first' and 'next' are the offsets corresponding to sarama.OffsetOldest and sarama.OffsetNewest, respectively.
func getStartOffset(k sarama.Client, topic string, first int64, next int64, t *monitor.Time) (int64, filterFunc, error) {
	// Get the unassigned case out of the way
	if t.IsUnassigned() {
		return sarama.OffsetOldest, nil, nil
	}
	// Handle the zero time and the case where the time is >= now
	isTime, tt := t.IsTime()
	if isTime {
		if tt.IsZero() {
			return sarama.OffsetOldest, nil, nil
		} else if !tt.Before(time.Now()) {
			return sarama.OffsetNewest, filterBeforeTime(tt), nil
		}
	}
	// Handle the case where the log is empty
	_, n := t.IsOffset()
	if first == next {
		if isTime || n >= 0 {
			return sarama.OffsetOldest, nil, nil
		}
		return sarama.OffsetOldest, filterFirstN(negate(n)), nil
	}
	// Handle the case where t is a time (necessarily strictly in the past)
	if isTime {
		// Sarama uses milliseconds since Jan 1 1970 UTC as the time format
		n, err := k.GetOffset(topic, partitionID, toKafkaTime(tt))
		if err != nil {
			return 0, nil, err
		}
		return n, filterBeforeTime(tt), nil
	}
	// Handle the case where the offset is non-negative
	if n >= 0 {
		n = next - n - 1
		if n <= first {
			n = sarama.OffsetOldest
		}
		return n, nil, nil
	}
	// Handle the case where the offset is negative (so into the future)
	return sarama.OffsetNewest, filterFirstN(negate(n)), nil
}

// filterForFinish returns a filter function to handle the finish time. Returns false if all messages would be filtered (so there is nothing to do). Here 'first' and 'next' are the offsets corresponding to sarama.OffsetOldest and sarama.OffsetNewest, respectively.
func filterForFinish(k sarama.Client, topic string, first int64, next int64, t *monitor.Time) (bool, filterFunc, error) {
	// Get the unassigned case out of the way
	if t.IsUnassigned() {
		return true, nil, nil
	}
	// Note the time or offset
	isTime, tt := t.IsTime()
	_, n := t.IsOffset()
	// Handle the case where t is a time strictly in the future
	if isTime && tt.After(time.Now()) {
		return true, filterAfterTime(tt), nil
	}
	// Handle the case where the log is empty
	if first == next {
		if isTime || n >= 0 {
			return false, nil, nil
		}
		return true, filterAfterN(negate(n)), nil
	}
	// Handle the case where t is a time (necessarily strictly in the past)
	if isTime {
		// Sarama uses milliseconds since Jan 1 1970 UTC as the time format
		n, err := k.GetOffset(topic, partitionID, toKafkaTime(tt))
		if err != nil {
			return false, nil, err
		} else if n == first {
			return false, nil, nil
		} else if n == sarama.OffsetNewest {
			n = next
		}
		return true, andFilter(filterAfterOffset(n), filterAfterTime(tt)), nil
	}
	// Handle the offset
	n = next - n
	if n <= first {
		return false, nil, nil
	}
	return true, filterAfterOffset(n), nil
}
