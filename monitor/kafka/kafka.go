// Kafka implements a client view of the Kafka storage.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/pool"
	"bitbucket.org/pcastools/rand"
	"context"
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
	"io"
)

// Connection represents a connection to Kafka.
type Connection struct {
	log.BasicLogable
	p         *pool.Pool       // The Kafka client pool
	formatter logger.Formatter // The formatter to convert log messages
}

// consumerOffsets is a topic internal to Kafka that we don't want to expose.
const consumerOffsets = "__consumer_offsets"

// partitionID is the default partion ID. Note: We don't currently support partitions.
const partitionID = 0

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// buildStreamData builds the data needed to read topic data from the Kafka client k, starting at start and ending at finish.
func buildStreamData(k sarama.Client, topic string, start *monitor.Time, finish *monitor.Time) (bool, int64, filterFunc, doneFunc, error) {
	// Get the range of available offsets
	first, err := k.GetOffset(topic, partitionID, sarama.OffsetOldest)
	if err != nil {
		err = fmt.Errorf("error getting oldest offset: %w", err)
		return false, 0, nil, nil, err
	}
	next, err := k.GetOffset(topic, partitionID, sarama.OffsetNewest)
	if err != nil {
		err = fmt.Errorf("error getting newest offset: %w", err)
		return false, 0, nil, nil, err
	}
	// Create the filter function to take account of the finish time
	ok, filter, err := filterForFinish(k, topic, first, next, finish)
	if err != nil {
		err = fmt.Errorf("error computing finish offset: %w", err)
		return false, 0, nil, nil, err
	} else if !ok {
		// The finish is earlier than the start, so the stream will be empty
		return false, 0, nil, nil, nil
	}
	// Compute the offset from which we should read log messages
	n, startFilter, err := getStartOffset(k, topic, first, next, start)
	if err != nil {
		err = fmt.Errorf("error computing offset for start: %w", err)
		return false, 0, nil, nil, err
	}
	// Combine the filter functions
	filter = andFilter(filter, startFilter)
	// Create the done function
	done, err := createDoneFunc(k, topic, first, next, finish)
	if err != nil {
		err = fmt.Errorf("error creating done function: %w", err)
		return false, 0, nil, nil, err
	}
	// Looks good
	return true, n, filter, done, nil
}

// errorLogger logs any errors reported by a partition consumer. Intended to be run in its own go-routine.
func errorLogger(errC <-chan *sarama.ConsumerError, lg log.Interface) {
	lg = log.With(lg, "[consumer]")
	for err := range errC {
		if err != nil {
			lg.Printf("%s", err)
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// Connection functions
/////////////////////////////////////////////////////////////////////////

// New connects to the Kafka server.
func New(options ...Option) (*Connection, error) {
	// Parse the user-provided options
	opts := parseOptions(options...)
	// Create the Sarama config
	// Note: We need at least Kafka 0.10 otherwise timestamps don't work
	cfg := sarama.NewConfig()
	cfg.Consumer.Return.Errors = true
	cfg.Version = sarama.V1_1_1_0
	// Create the connection pool
	p := pool.New(
		opts.Lifetime,
		func() (interface{}, error) {
			return sarama.NewClient(opts.BrokerAddrs, cfg)
		},
		func(x interface{}) {
			x.(io.Closer).Close()
		},
	)
	// Create the connection
	return &Connection{
		p:         p,
		formatter: opts.Formatter,
	}, nil
}

// Close closes the connection to Kafka.
func (c *Connection) Close() error {
	if c == nil || c.p == nil {
		return nil
	}
	return c.p.Close()
}

// getClientConnection returns a Kafka client connection from the pool.
func (c *Connection) getClientConnection() (sarama.Client, error) {
	x, err := c.p.Get()
	if err != nil {
		return nil, fmt.Errorf("error opening Kafka client connection: %w", err)
	}
	return x.(sarama.Client), nil
}

// reuseClientConnection returns the given Kafka client connection to the pool for reuse.
func (c *Connection) reuseClientConnection(k sarama.Client) {
	if !k.Closed() {
		c.p.Put(k)
	}
}

// wrap wraps the given function, providing it with a Kafka client connection from the pool and automatically returning it at the end of the call.
func (c *Connection) wrap(f func(k sarama.Client) error) func() error {
	return func() (err error) {
		// Fetch a Kafka client connection from the pool
		var k sarama.Client
		if k, err = c.getClientConnection(); err != nil {
			return
		}
		// Defer returning the connection to the pool
		defer func() {
			if err != nil {
				k.Close()
			} else {
				c.reuseClientConnection(k)
			}
		}()
		// Call the wrapped function
		err = f(k)
		return
	}
}

// IsLogName returns true iff the given string is a log name.
func (c *Connection) IsLogName(_ context.Context, name string) (bool, error) {
	// Sanity check
	if c == nil || c.p == nil {
		return false, errors.New("uninitialised connection")
	} else if name == consumerOffsets {
		return false, nil
	}
	// Fetch the topics
	var topics []string
	if err := c.wrap(func(k sarama.Client) (err error) {
		topics, err = k.Topics()
		return
	})(); err != nil {
		return false, err
	}
	// Check for the log name amongst the topics
	for _, t := range topics {
		if t == name {
			return true, nil
		}
	}
	return false, nil
}

// LogNames returns the log names.
func (c *Connection) LogNames(_ context.Context) ([]string, error) {
	// Sanity check
	if c == nil || c.p == nil {
		return nil, errors.New("uninitialised connection")
	}
	// Fetch the topics
	var topics []string
	if err := c.wrap(func(k sarama.Client) (err error) {
		topics, err = k.Topics()
		return
	})(); err != nil {
		return nil, err
	}
	// Remove the topic "__consumer_offsets"
	names := make([]string, 0, len(topics))
	for _, t := range topics {
		if t != consumerOffsets {
			names = append(names, t)
		}
	}
	return names, nil
}

// Stream returns an iterator down which entries from the log 'name' are passed. Only entries falling into the range determined by start and finish will be returned. If start is unassigned then the start of the log will be used; if end is unassigned then no end is used. The caller should ensure that the returned Iterator is closed, otherwise a resource leak will result.
func (c *Connection) Stream(_ context.Context, name string, start *monitor.Time, finish *monitor.Time) (monitor.Iterator, error) {
	// Sanity check
	if c == nil || c.p == nil {
		return nil, errors.New("uninitialised connection")
	}
	// Create a logger for this stream
	lg := log.PrefixWith(c.Log(), "[stream=%s]", rand.ID8())
	lg.Printf("Starting stream for \"%s\" (start: %s; finish: %s)...", name, start, finish)
	// Is there anything to do?
	if start.After(finish) {
		lg.Printf("Start after finish so empty stream")
		return monitor.EmptyIterator(), nil
	}
	// Create the topic iterator
	itr, err := c.newTopicIterator(name, start, finish, lg)
	if err != nil {
		lg.Printf("Unable to create topic data: %s", err)
		return nil, err
	}
	return itr, nil
}

// createTopicCloseFunc returns a close function that shuts down the Kafka consumers in the correct order.
func (c *Connection) createTopicCloseFunc(k sarama.Client, kc io.Closer, pc io.Closer) func() error {
	return func() error {
		// Close the partition consumer
		err := pc.Close()
		if err != nil {
			err = fmt.Errorf("error closing partition consumer: %w", err)
		}
		// Close the consumer
		if closeErr := kc.Close(); closeErr != nil {
			if err == nil {
				err = fmt.Errorf("error closing topic consumer: %w", err)
			}
		}
		// Close or reuse the client connection
		if err != nil {
			k.Close()
		} else {
			c.reuseClientConnection(k)
		}
		return err
	}
}

// newTopicIterator creates a new monitor.Iterator ranging over the indicated time period, from the given topic.
func (c *Connection) newTopicIterator(topic string, start *monitor.Time, finish *monitor.Time, lg log.Interface) (monitor.Iterator, error) {
	// Get a connection to the Kafka client
	k, err := c.getClientConnection()
	if err != nil {
		return nil, err
	}
	// Collect the data we'll need
	nonEmpty, n, filter, done, err := buildStreamData(k, topic, start, finish)
	if err != nil {
		k.Close()
		return nil, err
	} else if !nonEmpty {
		c.reuseClientConnection(k)
		lg.Printf("Start after finish so empty stream")
		return monitor.EmptyIterator(), nil
	}
	// Create a consumer
	kc, err := sarama.NewConsumerFromClient(k)
	if err != nil {
		k.Close()
		return nil, fmt.Errorf("error creating topic consumer: %w", err)
	}
	// Make a partition consumer for the partition containing the log
	pc, err := kc.ConsumePartition(topic, partitionID, n)
	if err != nil {
		kc.Close()
		k.Close()
		return nil, fmt.Errorf("error creating partition consumer: %w", err)
	}
	lg.Printf("Consuming from offset %d...", n)
	// Create the iterator and set the logger
	itr := &topicIterator{
		msgC:      pc.Messages(),
		filter:    filter,
		done:      done,
		formatter: c.formatter,
		closeFunc: c.createTopicCloseFunc(k, kc, pc),
	}
	itr.SetLogger(lg)
	// Start the error logger running
	go errorLogger(pc.Errors(), itr.Log())
	// Return the iterator
	return itr, nil
}
