// Tests of format.go.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package logger

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestDefaultFormat tests the default format.
func TestDefaultFormat(t *testing.T) {
	assert := assert.New(t)
	// Converts a series of log messages to strings and back to log messages,
	// and check that we get what we expect
	tests := []Message{
		{
			Identifier: "hello",
			Message:    "This is a message",
		},
		{
			Identifier: "a:b",
			Message:    "Our DATA: is silly",
		},
		{},
		{
			Data: map[string]interface{}{
				"a": float64(2),
				"b": "hello",
			},
		},
		{
			Identifier: "joe:blog:is\\:here",
			Message:    "Our DATA: is still \\DATA: silly",
			Data: map[string]interface{}{
				"a": float64(2),
				"b": "hello",
				"c": []interface{}{"1", "2", "3"},
			},
		},
	}
	msg2 := Message{}
	for _, msg := range tests {
		s, err := ToString(msg, DefaultFormat)
		assert.NoError(err)
		err = DefaultFormat.Scan(&msg2, s)
		assert.NoError(err)
		assert.Equal(strings.TrimSpace(msg.Identifier), msg2.Identifier)
		assert.Equal(strings.TrimSpace(msg.Message), msg2.Message)
		assert.Equal(msg.Data, msg2.Data)
	}
}
