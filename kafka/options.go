// Options describes the various options for a Kafka connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kafka

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcastools/log"
	"strings"
)

// Option sets options on a Kafka logger.
type Option interface {
	apply(*kafkaOptions)
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*kafkaOptions)
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *kafkaOptions) {
	h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*kafkaOptions)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// kafkaOptions are the options on a Kafka logger.
type kafkaOptions struct {
	Formatter    logger.Formatter // The formatter for the log messages
	BrokerAddrs  []string         // The broker addresses for the Kafka producers
	BufferSize   int              // The size of the message buffer
	NumProducers int              // The number of Kafka producers
	NumWorkers   int              // The number of workers feeding log messages to the producers
	Log          log.Interface    // The destination log for debugging
}

// The default option values.
const (
	DefaultBrokerAddr   = "localhost:9092" // The default broker address to use if no broker addresses are specified.
	DefaultBufferSize   = 1024
	DefaultNumProducers = 1
	DefaultNumWorkers   = 2
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isInSlice returns true if and only if s is contained in the slice S.
func isInSlice(s string, S []string) bool {
	for _, t := range S {
		if s == t {
			return true
		}
	}
	return false
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) *kafkaOptions {
	// Create the default options
	opts := &kafkaOptions{
		Formatter:    logger.DefaultFormat,
		BrokerAddrs:  make([]string, 0, 1),
		BufferSize:   DefaultBufferSize,
		NumProducers: DefaultNumProducers,
		NumWorkers:   DefaultNumWorkers,
		Log:          log.Discard,
	}
	// Set the options
	for _, h := range options {
		h.apply(opts)
	}
	// If no brokers have been specified, use the default broker address
	if len(opts.BrokerAddrs) == 0 {
		opts.BrokerAddrs = append(opts.BrokerAddrs, DefaultBrokerAddr)
	}
	// Ensure that the number of producers doesn't exceed the number of workers
	if opts.NumProducers > opts.NumWorkers {
		opts.NumProducers = opts.NumWorkers
	}
	return opts
}

// Formatter sets the formatter to use when converting log messages to strings.
func Formatter(f logger.Formatter) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if f == nil {
			f = logger.DefaultFormat
		}
		opts.Formatter = f
	})
}

// AddBrokerAddr adds the given addresses to the slice of Kafka broker address. Broker addresses should be in the format "hostname:port". DefaultBrokerAddr if and only if no broker addresses are added.
func AddBrokerAddr(addr ...string) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		for _, s := range addr {
			s = strings.TrimSpace(s)
			if len(s) > 0 && !isInSlice(s, opts.BrokerAddrs) {
				opts.BrokerAddrs = append(opts.BrokerAddrs, s)
			}
		}
	})
}

// BufferSize sets the size of the message buffer.
func BufferSize(size int) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if size < 0 {
			size = 0
		}
		opts.BufferSize = size
	})
}

// NumProducers sets the number of Kafka producers to create.
func NumProducers(n int) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if n < 1 {
			n = 1
		}
		opts.NumProducers = n
	})
}

// NumWorkers sets the number of workers to create. The workers are responsible for feeding log messages to the Kafka producers. A good rule of thumb is to create twice as many workers as producers.
func NumWorkers(n int) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if n < 1 {
			n = 1
		}
		opts.NumWorkers = n
	})
}

// DebugLog sets the logger to use for debugging information.
func DebugLog(l log.Interface) Option {
	return newFuncOption(func(opts *kafkaOptions) {
		if l == nil {
			l = log.Discard
		}
		opts.Log = l
	})
}
